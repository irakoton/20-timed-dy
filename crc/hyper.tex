\input{fig_formula}

\section{Security (Hyper)properties} \label{sec:time-security}

  \subsection{A Hyperlogic}
    We finally present a logic for specifying hyperproperties of protocols.
    Its key novelty compared to similar logics is its ability to express properties about the adversary, through more developed atomic formulae and quantifiers.

    % \paragraph{Formulae}
    \begin{definition}[Formula]
      The set of \emph{\pictl[Hyper] formulae} is given by the grammar below:
      \[\begin{array}{rlll@{\qquad}l}
        \varphi\ ::=\ 
          & \forall \pi. \varphi & \forall x. \varphi & \forall X. \varphi & \text{\descrfont quantifiers} \\
          & \varphi \ltluntil \varphi & & & \text{\descrfont until} \\
          & \varphi \wedge \varphi & \neg \varphi & & \text{\descrfont logical operators} \\
          & X \deduce[\pi] v & u \deduce[\pi] v & \alpha_\pi & \text{\descrfont atomic formulae}
          % & \alpha_\pi & & \text{\descrfont action} \\
          % & \xi \deduce[\pi] u & & \text{\descrfont computability}
      \end{array}\]
      with \(\pi\) called a \emph{path variable}, \(X \in \X[2]\) is called a \emph{second-order variable}, \(x \in \X\), \(\alpha\) is an action (as in the semantics), and \(u,v\) are terms.
    \end{definition}

    The ``until'' operator \(\varphi \ltluntil \psi\) means that \(\varphi\) should hold continuously from the current time \(t\), and for a duration \(\delta\), while \(\psi\) holds at time \(t+\delta\).
    It is not required that \(\varphi\) holds at time \(t\) or \(t+\delta\).
    \emph{Path quantifiers} \(\forall \pi\) then quantify over all traces of the studied process.
    The quantification is performed from the current state:
    for example, \(\forall \pi. (\varphi \ltluntil \exists \pi'. \psi)\) expresses that \(\varphi\) should hold all along any trace \(\pi\), until a point where \(\pi\) could potentially branch to a trace \(\pi'\) verifying \(\psi\).
    % 
    The logical operators \(\wedge\) and \(\neg\) have the expected semantics.
    The quantifiers over computations are also key features (\(\forall x\) for terms, \(\forall X\) for recipes).
    The atomic formulae of the logic are finally \(\xi \deduce[\pi] u\), (``the computation \(\xi\) results in \(u\) with access to the frame of \(\pi\)''), and \(\alpha_\pi\) (``the action \(\alpha\) occurs at the current time'').
    The case \(\alpha = \silent\) is also used to express that no particular action occurs.

    % Very elementary examples include the formula \(\atk(x) = \exists X, X \deduce[\pi] x\) used in the motivating example in Section~\ref{sec:motivating} stating that \(x\) is deducible from the outputs of the trace \(\pi\), but also a way to encode equality tests;
    % if \(\pi\) is an arbitrary path variable and \(u,v\) are terms:
    % \[\exists x, u \deduce[\pi] x \wedge v \deduce[\pi] x\]
    % encodes that \(x\) are equal modulo rewriting.
    % 
    % \paragraph{Formula satisfiability}
      Figure~\ref{fig:time-formula} then formalises when a process \(P\) satisfies a \pictl[Hyper] formula \(\varphi\). 
      This is done through judgements of the form
      \(\Pi \models \varphi\)
      where \(\varphi\) is a formula and \(\Pi\) is a substitution from path variables to traces.
      We assume in particular a dedicated path variable \(\epsilon \in \dom(\Pi)\) used to track the last path quantifier in scope.
      We also write, to restrict all traces of \(\Pi\) to their suffix starting at time \(t\): 
      \[\Pi \stamp{t} = \{\pi \mapsto \pi(\Pi)\stamp{t} \mid \pi \in \dom(\Pi)\}\,.\]

      \begin{definition}[satisfiability]
        A process \(P\) \emph{satisfies} a formula \(\varphi\), written \(P \models \varphi\), when \(\{\epsilon \mapsto \epsilon_\infty\} \models \varphi\), where \(\epsilon_\infty\) is an infinite, temporally structured sequence of \ruletic transitions starting from \(P\).
      \end{definition}

  \subsection{Useful Syntax Extensions} \label{sec:extensions}
    We now present syntax extensions encodable into our logic, that will be convenient for our case studies.
    We recall that, beyond classical logical operators (\(\exists\), \(\vee\), etc.), we already introduced in the motivating example the \(\progress\) formula, and \(\atk(x)_\pi \eqdef \exists X, X \deduce[\pi] x\) expressing the non-secrecy of \(x\) at the current time in \(\pi\).
    
    \paragraph{Temporal Operators}
      The most common syntax extensions of temporal logics are the following additional temporal operators, some of them appearing in the motivating example:
      \begin{mathpar}
        \ltlfinally \varphi = \top \ltluntil \varphi \and 
        \ltlglobally \varphi = \neg(\ltlfinally \neg \varphi) \and 
        \varphi \ltlweakuntil \psi = (\varphi \ltluntil \psi) \vee \ltlglobally \varphi \and 
        \varphi \ltlrelease \psi = \neg(\neg\varphi \ltluntil \neg\psi) = \psi \ltlweakuntil (\varphi \wedge \psi)
      \end{mathpar}
      Intuitively, \(\ltlfinally \varphi\) (``finally \(\varphi\)'') means that \(\varphi\) will eventually be true.
      Its dual operator \(\ltlglobally \varphi\) (``globally \(\varphi\)'') expresses that \(\varphi\) is continuously true.
      Typically, the formula \(\ltlfinally \ltlglobally \silent_\pi\) is satisfied by finite traces, i.e., traces as usually considered in protocol analysis that only perform a finite number of non-silent actions.
      The relaxed variant of ``until'', \(\varphi \ltlweakuntil \psi\) (``\(\varphi\) weak until \(\psi\)''), allows \(\varphi\) to hold forever in case \(\psi\) never holds.
      The \(\varphi \ltlrelease \psi\) (``\(\varphi\) release \(\psi\)'') has a dual semantics:
      at time \(t\), it states that \(\psi\) should hold at any time \(t+\delta\) such that \(\varphi\) is continuously false in the window \((t,t+\delta)\).
      I.e., the first occurrence of \(\varphi\) ``releases'' the obligation of \(\psi\) to hold, hence its equivalence with \(\psi \ltlweakuntil (\varphi \wedge \psi)\).

      It is also possible to consider, as in real-time extensions of \ltl such as \mitl[(Hyper)]~\cite{AFH96,HZJ20}, constrained versions of the temporal operators.
      The formula \(\varphi \ltluntil[I] \psi\), where \(I\) is a real interval, means that \(\varphi\) should hold during a time lapse \(\delta \in I\), and \(\psi\) holds after that.
      Therefore \(\varphi \ltluntil \psi\) is equivalent to \(\varphi \ltluntil[(0,+\infty)] \psi\).
      The analogues \(\ltlfinally[I] \varphi = \top \ltluntil[I] \varphi\), \(\ltlglobally[I] \varphi = \neg \ltlfinally[I] \neg \varphi\) and \(\varphi \ltlrelease[I] \psi = \neg(\neg \varphi \ltluntil[I] \neg \psi)\) can naturally be defined.
      For example, at time \(t\), the formula \(\varphi \ltlrelease[I] \psi\) is notably read as ``\emph{\(\psi\) should hold at any moment \(t+\delta\), \(\delta \in I\), except maybe when \(\varphi\) held at some time \(t' \in (t,t+\delta)\)}''.
      Such interval constraints can most often be encoded as time conditions in processes and are therefore not necessary in our framework from an expressivity standpoint.
      We however describe in the technical report~\cite{tech_report} a blockchain atomic swap protocol where this operator is convenient to express properties of the form ``\emph{the protocol is fair for all participants reacting fast enough}''.
    
    \paragraph{Indistinguishability}
      To express stronger notions of secrecy than what is possible using the \(\atk\) formula, we can formalise \emph{process indistinguishability}.
      Typically, to model that no information can be extracted about the parameter \(x\) of a process \(P(x)\), we require that the executions of \(P(x_0)\) and \(P(x_1)\) are indistinguishable from the adversary's view for any term \(x_0\) and \(x_1\).
      Formally, two traces are indistinguishable if, first, they perform the same interactions with the attacker (inputs and outputs):
      \begin{align*}
        \begin{array}{r@{}l}
          \varphi_A(\pi,\pi')\ \eqdef\ 
          \forall X. \forall Y.\
          & (\InP[X]{Y}_\pi \Leftrightarrow \InP[X]{Y}_{\pi'}) \\
          \wedge\ & (\OutP[X]{Y}_\pi \Leftrightarrow \OutP[X]{Y}_{\pi'})
        \end{array}
      \end{align*}
      and if, second, the adversary cannot compute an equality test (started as early as possible and finished by the current time) that holds in one process and not on the other.
      This would indeed permit to distinguish a black-box access to one process against the other by effectively computing this test.
      This is expressed by the formula:
      \begin{align*}
        \begin{array}{r@{}l}
          \varphi_E(\pi,\pi')\ \eqdef\ 
          \forall X. \forall Y.\ X \StatEq[\pi] Y \Leftrightarrow X \StatEq[\pi'] Y
        \end{array}
      \end{align*}
      where \(X \StatEq[\pi] Y \eqdef \exists x, X \deduce[\pi] x \wedge Y \deduce[\pi] x\).
      We would then call \emph{static equivalence} the conjunction of these two formulae:
      \[\pi \StatEq \pi' \eqdef \varphi_A(\pi,\pi') \wedge \varphi_E(\pi,\pi')\]
      
    % \paragraph{Active Indistinguishability}
      Process indistinguishability itself may then be modelled by \emph{trace equivalence}~\cite{CKR18}. 
      % , and recalling the classical notion of non-interference~\cite{CS10a}. 
      It states that for each trace of any of two processes \(P_1,P_2\), there is a statically-equivalent trace in the other.
      We use:
      \[P = (\EventP\ \leftfun; P_1) + (\EventP\ \rightfun; P_2)\]
      % to encode path quantifiers over \(P_1,P_2\), with \(\leftfun,\rightfun \in \sig_0\) fresh constants identifying which process is non-deterministically chosen to be executed.
      to encode path quantifiers over multiple processes \(P_1,P_2\), with \(\leftfun,\rightfun \in \sig_0\).
      Trace equivalence of \(P_1\) and \(P_2\) then rephrases as:
      \[P \models \ \forall \pi_1. \exists \pi_2. (\ltlfinally \leftfun_{\pi_1} \Leftrightarrow \ltlfinally \rightfun_{\pi_2}) \wedge \ltlglobally(\pi_1 \StatEq \pi_2)\]
      This can be generalised to quantify over an arbitrary number of processes, at least regarding path quantifiers not under the scope of an ``until''.
      Under this restriction, we will thus use a syntactic sugar
      \(\forall \pi \colon\! Q.\, \varphi\)
      to quantify specifically over the traces of \(Q\).
      Going back to the motivating example, we then model the (strong) fairness towards \(A\) using indistinguishability, by considering:
      % , which is stronger than the \(\atk\) formula of Section~\ref{sec:motivating}.
      \begin{align*}
        P_i =\ & \InP{x_0}; \InP{x_1}; \InP{d}; A(x_i,d) & i \in \{0,1\}, d \in \TX
      \end{align*}
      We then require that for all accepting executions \(\pi_0\) of \(P_0\), there exists an execution \(\pi_1\) of \(P_1\) that is indistinguishable from \(\pi_0\) until the event \(\waitfun\) occurs.
      This is expressed by:
      \begin{align*}
        & \forall \pi_0 \colon\! P_0.\, \exists \pi_1 \colon\! P_1.\, \forall z. \ltlfinally \acceptfun(z)_{\pi_0} \Rightarrow \pi_0 \StatEq \pi_1 \ltluntil \waitfun_{\pi_0}
      \end{align*}

  \subsection{Fragments of Interest} \label{sec:fragments}
    As the design of our logic has been inspired by \ctl[Hyper]~\cite{CFK14}, we can derive several sublogics analogous to its classical fragments.
    % The first of interest is the following: 

    \begin{definition}%[{\piltl[Hyper]}]
      \piltl[Hyper] contains the formulae in \emph{prenex form}, i.e., \(Q_1 \pi_1 \ldots Q_n \pi_n. \varphi\), \(Q_i \in \{\forall, \exists\}\), \(\varphi\) without path quantifiers.
    \end{definition}

    It notably encompasses indistinguishability properties, but not liveness.
    % Note that, when checking the satisfaction \(\Pi\models \varphi\) of a \piltl[Hyper] formula \(\varphi\), the trace \(T\) is irrelevant and can be left implicit.
    Another important fragment is the non-relational fragment \pictl, that is, the set of properties that do not express relations between different traces (through shared variables):

    \begin{definition}%[\pictl]
      \pictl contains the formulae \(\varphi\) such that for all subformulae of \(\varphi\) of the form \(\forall \pi. \psi\), all (path, regular, second-order) variables in \(\psi\) that are not \(\pi\) are bound by a quantifier in \(\psi\).
    \end{definition}
    
    This fragment is for example sufficient to express all properties of our motivating example, but not indistinguishability, making it incomparable to \piltl[Hyper].
    % Note that, when checking the satisfaction \(\Pi\models \varphi\) of a \piltl[Hyper] formula \(\varphi\), the mapping \(\Pi\) is irrelevant and can be left implicit.
    % \piltl[Hyper] and \pictl are hence incomparable, as the former cannot express liveness.
    % trace equivalence cannot be expressed in \pictl, and liveness properties cannot be expressed in \piltl[Hyper].
    However, both subsume safety properties, also targeted by our automated proofs (Section~\ref{sec:implem}):

    \begin{definition}%[\piltl]
      \piltl contains the formulae of the form \(\forall \pi. \varphi\) where \(\varphi\) does not contain path quantifications.
    \end{definition}

    In addition of the examples of safety properties presented all across this paper,
    \piltl can also naturally express \emph{resilience} assumptions on a channel \(c\), as used in some analyses of fair non-repudiation protocols~\cite{BDK17}.
    This intuitively means that, during a trace \(\pi\), network manoeuvres are made to ensure that any message sent on \(c\) is received eventually.
    % \[\ltlglobally (\forall X,Y, (\OutP[X]{Y}_\pi \wedge X \deduce[\pi] c) \Rightarrow \ltlfinally \InP[X]{Y}_\pi)\]
