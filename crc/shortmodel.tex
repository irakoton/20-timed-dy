\subsection{Cryptographic Primitives and Messages}
  \paragraph{Term Algebra}
    We first review the algebraic setting we use to model cryptographic primitives and protocol messages. 
    All the material is standard, except for the notions of time and cost, which are new. 
    We start with \emph{atomic values}:
    \begin{align*}
      \atomic = \CST \uplus \N \uplus \X \uplus \setR[+] 
      & & \text{and with } \TX \subseteq \X
    \end{align*}
    % that are used as building block to construct messages.
    The infinite set \(\CST\) of \emph{constants} is used to model public values such as identities, or any value known to the adversary.
    On the contrary, the infinite set \(\N\) of \emph{names} models private values such as nonces or long-term keys.
    The infinite set \(\X\) contains the \emph{variables}, including a distinguished infinite subset \(\TX\) of \emph{temporal variables} used to specifically bind time values, represented by elements of \(\setR[+]\). 

    In particular, the set of atomic values is implicitly typed: some values represent time, others cryptographic material. 
    For the sake of readability, we gloss over this distinction and implicitly assume that all incoming notions (terms, substitutions, etc.) are well-typed. 
    Next, we introduce a notion of \emph{signature}, that is a finite set
    \[\sig=\{\ffun[x_1, \ldots, x_n],\ \gfun[x_1, \ldots, x_m],\ \hfun[x_1, \ldots, x_p], \ldots\}\]
    used to represent operations such as cryptographic primitives.
    A symbol \(\ffun[x_1, \ldots, x_n]\) models a primitive taking \(n\) arguments materialised by the variables \(x_i\), the temporal variables among them indicating time parameters.
    A \emph{cost} for \(\ffun\) is then an arithmetic expression \(\cost(\ffun)\) over temporal variables among \(x_1, \ldots, x_n\).
    The cost of a function can be seen as a static amount of time necessary to compute it, regardless of its result.
    From all this, we can then define a standard notion of \emph{term} to model computations.

    \begin{definition}[Term]
      A \emph{term} is an atomic value \(a \in \atomic\), or a function symbol \(\ffun\) or an arithmetic operator (\(+\), \(\times\), \ldots) applied to other terms while respecting arities and types. 
      We write \(\termset(S)\) the set of terms built from the atomic values and functions of \(S \subseteq \atomic \cup \sig\).
      % , and specifically say that \(t \in \termset(S)\) is a \emph{constructor term} if \(S \cap \sigd = \emptyset\). % and \emph{ground} if \(S \cap \X = \emptyset\), and \emph{public} if \(S \cap \N = \emptyset\).\gb{we should define here what a destructor term is}
    \end{definition}

    Finally, the notion of \emph{substitution} \(\sigma\) is defined as usual, i.e.,
    \[\sigma = \{x_1 \mapsto u_1, \ldots, x_n \mapsto u_n\}\] 
    is a mapping where \(x_1,\ldots,x_n\) are pairwise distinct variables forming the \emph{domain} of \(\sigma\) written \(\dom(\sigma)\), and \(u_1, \ldots, u_n\) are terms. 
    The application of a substitution \(\sigma\) to a term \(t\) is denoted by \(t\sigma\) which is, informally, the term obtained by simultaneously replacing all occurrences of \(x_i\) in \(t\) by \(u_i = x_i \sigma\). 
    Regarding notations, we write \(\sigma \cup \sigma'\) the substitution of domain \(\dom(\sigma) \cup \dom(\sigma')\) that extends both \(\sigma\) and \(\sigma'\) (provided they coincide on \(\dom(\sigma) \cap \dom(\sigma')\));
    %
    and \(\sigma\sigma'\) refers to the composition \(\sigma' \circ \sigma\), i.e., for all terms \(t\), \(t(\sigma\sigma') = (t\sigma) \sigma'\).

  \paragraph{Rewriting}
    Terms have an operational behaviour (including the execution time) defined by a rewriting system that we augment with a cost function. 

    \begin{definition}[Timed rewriting]
      A \emph{(timed) rewriting system} \(\R\) is a finite set of triples \((\ell,d,r)\), with \(\ell,r\) terms that do not contain names, and \(d\) an arithmetic expression over temporal variables of \(\ell\).
      We call such triples (timed) \emph{rewrite rules}, written
      \[\ell \rewrite[d] r\,.\]
    \end{definition}
    
    We call \(d\) the \emph{cost} of the rule (omitted when null).
    It intuitively indicates the execution time of the corresponding computation.
    In particular, a rewriting system \(\R\) induces a rewriting relation \(\rewrite[d]_\R\) that is the closure of \(\R\) under substitution and context. 
    That is:
    \begin{itemize}
      \item \(\ell\sigma \rewrite[d\sigma] r\sigma\) for any \((\ell \rewrite[d] r) \in \R\) and substitution \(\sigma\);
      \item \(C[u] \rewrite[d] C[v]\) if \(u \rewrite[d] v\) and \(C[ \cdot]\) is a linear context, i.e., an application of function symbols on top of a term.
    \end{itemize}

    In practice, we only consider rewriting systems defining convergent computations for simplicity and coherence of our definitions:

    \begin{definition}[Normal form and Convergence]
      A term \(t\) is said to be in \emph{normal form} if it cannot be reduced by \(\rewrite_\R\).
      A rewriting system is \emph{convergent} if for all terms \(t\), there exists a unique term in normal form, written \(t \norm\), such that \(t \rewrite[d_1]_\R \cdots \rewrite[d_n]_\R t \norm\).
    \end{definition}

    Symbolic models also sometimes consider (unoriented) \emph{equations} to specify algebraic properties such as associativity and commutativity, typically useful to model \(\oplus\).
    As they would be untimed in our context and are irrelevant to all security properties we consider, we omit them from the model for the sake of succinctness.
