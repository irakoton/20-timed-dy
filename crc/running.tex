\section{Motivating Example} \label{sec:motivating}
  
  Timed commitments~\cite{BN00} are a classic tool to build fair protocols. 
  In this section, we give a first insight of our model by outlining the construction and verification of the fair sampling protocol briefly described in the introduction~\cite{BN00}.

  Recall that commitments are cryptographic primitives allowing a party to commit to a value while keeping it hidden from other parties, until a moment of their choice. 
  Timed commitments enhance this scheme with an additional algorithm for revealing the committed value within some time \(d\), even if the committer refuses to open its commitment. 
  We model them by \emph{function symbols}, written:
  \begin{align*}
    \commit[x,y,d] & & 
    \open[x,y,z] & & 
    \force[x] & & 
    \testfun[x,d]
  \end{align*}
  Variables named as \(x,y,z,\ldots\) stand for arguments serving as cryptographic material, while \(d,t\) stand for durations and time parameters.
  The expression \(\commit(m,r,d)\) for example represents a commitment of the message \(m\) that can be opened with the opening data \(r\), or forced in time \(d\). 
  The opening of a commitment \(c\) with \(m,r\) is represented by \(\open(c,m,r)\), whereas \(\force(c)\) is a forced recovery of \(m\) from \(c\).
  Finally, \(\testfun(m,d)\) tests that the bitstring \(m\) is a well-formed commitment of time parameter \(d\).
  We express the operational behaviour of these expressions by \emph{timed rewriting rules}:
  \[\begin{array}{r@{\ }l}
    \open(\commit(x,y,d),x,y) & \rewrite \okfun \\
    \testfun(\commit(x,y,d),d) & \rewrite \okfun \\
    \force(\commit(x,y,d)) & \rewrite[d] x
  \end{array}\]
  The first rule models the correctness of commitments: 
  opening \(\commit(x,y,d)\) with the correct data instantaneously yields validation.
  The second rule simply states that one can test that a message is valid commitment parametrised with \(d\) (without opening it).
  Finally, the last rule models a forced opening:
  the message \(x\) can always be recovered in time \(d\).
  This is indicated by the \(+d\) label, whereas other rules are implicitly labelled \(+0\).
  The absence of other rules models that no information can be extracted about the committed message without knowing the opening data, or spending \(d\) units of time forcing it.
  % 
  This scheme can be used in a string-sampling protocol, as the exclusive or (xor, \(\oplus\)) of two bitstrings \(x\) and \(y\) held by parties \(A\) and \(B\), respectively.
  % For simplicity, we omit the classical symbolic modelling of \(\oplus\) by unoriented group equations, as it not specific to the timed setting.
  Its goal is to ensure that no party can bias the outcome, i.e., \(x \oplus y\) is uniformly distributed if at least \(A\) or \(B\) follows the protocol and samples its bitstring uniformly:
  % The normal protocol flow is, in informal Alice-Bob notation:
  \[\begin{array}{ll}
    & A \to B:\ \commit(x,r,d) \\
    & B \to A:\ y \\
    & A \to B:\ x,r
  \end{array}\]
  After this exchange, \(A\) and \(B\) can compute \(x \oplus y\).
  However, to ensure that the result is unbiased, several additional precautions have to be taken.
  We formalise in our model the views of each party below.

  \paragraph{View of A}
    First, \(A\) generates a fresh nonce \(r\) and outputs the commitment \(\commit(x,r,d)\) at time \(t\).
    Then, it waits for a response \(y\), but only accepts it if received at some time \(t' < t + d\).
    This way, \(B\) is too short on time to force the commitment, and can therefore not compute \(y\) depending on the retrieved value of \(x\). 
    If \(A\) received \(y\) in time, it accepts \(x \oplus y\) as the protocol's result and reveals \(x\) and \(r\).
    We express this process in a timed extension of applied \(\pi\)-calculus:
    \[\begin{array}{r@{\ }l}
      A(x,d) =\ 
      & \NewP r;\ \OutP{\commit(x,r,d)} \stamp{t};\\ 
      & \EventP\ \waitfun\stamp{t_s}\ \WhenP\ t_s < t + d; \\ 
      & \InP{y} \stamp{t'}\ \WhenP\ t' < t + d;\\
      & \EventP\ \acceptfun(x \oplus y); \OutP{x}; \OutP{r}; 0.
    \end{array}\]
    Inputs and outputs are materialised by \(\InP{\cdot}\) and \(\OutP{\cdot}\) instructions, while 0 simply indicates a terminated process.
    Timestamps and time conditions are expressed using the \(\stamp{}\) and \(\WhenP\) operators.
    Finally, the events (\(\waitfun\) and \(\acceptfun\)) are not actual instructions of the protocol:
    they should rather be seen as meta-events identifying specific phases of the protocol, for formalising security properties.
    Here for example, \(\waitfun\) indicates that \(A\) is still waiting for \(B\)'s reply at time \(t_s\).
    The expected security property is then, informally:
    \begin{quote}
      \itshape Fairness towards \(A\):
      the bitstring \(x\) emitted by \(A\) remains secret until \(B\) responds (if it does).
    \end{quote}
    Note that \emph{timed} commitment are only beneficial to \(B\), i.e., the property would hold for a protocol relying on a regular commitment.
    We formalise the property by requiring that no computation \(X\) of the adversary (here \(B\)) may result in \(x\) while \(A\) still awaits an answer.
    We express this in our model by the following \emph{\pictl formula}:
    \begin{align*}
      & \forall \pi. \forall z. \ltlfinally \acceptfun(z)_\pi \Rightarrow \neg \atk(x)_\pi \ltluntil \waitfun_\pi
    \end{align*}
    The syntactic sugar \(\atk(x)_\pi \eqdef \exists X, X \deduce[\pi] x\) is a formula intuitively expressing that \(x\) is computable with access to the outputs sent by \(A\) during an execution \(\pi\).
    The operators \(\ltlfinally\) and \(\ltluntil\) are respectively read as ``finally'' and ``until'' and the property itself thus intuitively means ``\emph{for all executions \(\pi\) of \(A(x,d)\) that eventually accepts a result \(z\), the attacker \(B\) cannot compute \(x\) before the event \(\waitfun\)}''.
    Note in particular how the quantification \(\forall\pi\) requires to consider executions where the \(\waitfun\) event occurs arbitrarily close to the reception of \(y\).
    % 
    We also later formalise stronger versions of this property exploiting the full expressivity of our logic for hyperproperties.


  \paragraph{View of B}
    Dually, \(B\) awaits for a value \(x_c\), and upon reception checks that it is a valid commitment.
    It then awaits for \(x\) and \(r\), and attempts to open \(x_c\) with these values. 
    If everything proceeds as expected, \(B\) accepts the value \(x \oplus y\);
    otherwise, it forces the commitment through process \(F = \EventP\ \acceptfun(\force(x_c) \oplus y); 0\). %and accepts the value \(\force(x_c) \oplus y\).
    \[\begin{array}{@{}r@{\ }l@{}}
      B(y,d) =\ 
      & \InP{x_c};\ \IfP\ \testfun(x_c,d) = \okfun\ \ThenP \\
      & \OutP{y};\ \EventP\ \waitfun; (F + \InP{x}; (F + \InP{r}; C))\\[1mm]
      % 
      C =\ 
      % & \InP{r}; 
      & \IfP\ \open(x_c,x,r) = \okfun\ \ThenP\ \EventP\ \acceptfun(x \oplus y); 0\ \ElseP\ F
    \end{array}\]
    % The test ``\(x_c = \commit(\open(x_c,r),r,d)\)'' is used to check the success of the opening phase.
    % Indeed, this algebraic equation holds only if \(x_c\) is of the form \(\commit(m,r,d)\) for some \(m\), 
    % Naturally, real timed commitment schemes will expectedly implement a dedicated and more natural operation to perform this test, that is however not needed in our logical view of the protocol.
    % which is sufficient in our logical view of the protocol.
    A sum \(P + Q\) indicates an (external) non deterministic choice, i.e., \(B\) may execute either of two process options \(P\) or \(Q\).
    In particular, \(B\) always has the choice to force the commitment instead of waiting for further opening data.
    The desired security property is then:
    \begin{quote}
      \itshape Fairness towards \(B\):
      after \(B\) sent its bitstring \(y\), the protocol can always proceed until the end, even if \(A\) aborts.
    \end{quote}
    A classical approach to formalise such \emph{liveness} properties is to assume that \(B\) always executes instructions until being stuck to wait for an input from \(A\).
    In practice, the \pictl formula 
    \[\stuck = \forall \pi.\, \silent_\pi \ltlweakuntil \exists X.\, \InP{X}_\pi\]
    captures this idea that \(B\) cannot progress.
    Indeed, \(\silent_\pi\) can be understood here as ``\emph{the execution \(\pi\) is pending}'', while \(\varphi \ltlweakuntil \psi\) (``weak until'') is a relaxed variant of \(\varphi \ltluntil \psi\) allowing \(\varphi\) to hold forever, should \(\psi\) never be true.
    This lets the \(\stuck\) formula be read as ``\emph{the first action (if any) of any potential executions \(\pi\) starting from the current state is an input from \(A\)}''.
    Therefore, the following formula:
    \[\progress = \ltlglobally (\neg \stuck \Rightarrow \ltlfinally \stuck)\]
    expresses that \(B\) always keeps going until being stuck,
    as \(\ltlglobally \varphi\) (``globally'') means that \(\varphi\) is true all along the protocol execution.
    % , and \(\ltlfinally \varphi\) (``finally'') that \(\varphi\) is eventually true.
    % 
    Altogether, fairness towards \(B\) is captured by the following formula:
    \begin{align*}
      & \forall \pi.\, \progress \Rightarrow \ltlglobally (\waitfun_\pi \Rightarrow \ltlfinally \exists z.\, \acceptfun(z)_\pi)
    \end{align*}
    The second part of the formula is read as ``\textit{at any point, if the event \(\waitfun\) occurs in the execution \(\pi\), then \(\acceptfun(z)\) will occur later}''.

  \paragraph{Rest of the paper}
    In the following sections, we formalise 
    % the syntax and operational semantics of 
    the timed extension of applied \(\pi\)-calculus used above.
    We also introduce a rich language for expressing security properties, \pictl[Hyper], generalising the simple (non-relational) formulas seen so far, and illustrate their use by modelling several protocols of interest in our framework.
    We then study the decidability of verifying that a protocol with a fixed number of sessions satisfies a \pictl[Hyper] formula.
    We show that the problem is undecidable in general despite boundedness, but can be reduced to forms of constraint solving.
    Finally, we formalise a \tamarin-based approach to prove fairness towards \(A\), as well as properties of other relevant protocols.
