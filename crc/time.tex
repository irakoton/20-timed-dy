\section{Applied \texorpdfstring{\(\pi\)}{pi}-Calculus with Time}
  We first formalise our extension of the applied \(\pi\)-calculus including a support for timed cryptography.
  % We refer to a parallel read with the motivating example for illustration.

  \input{shortmodel.tex}

  \subsection{Protocols}
    \input{fig_computability}

    \paragraph{Syntax}
    We model distributed protocols through a notion of \emph{(timed) process}, mostly borrowed from the applied \(\pi\)-calculus, except that instructions may have timestamps and time conditions. 
    These conditions are typically built from classical arithmetic or logical operators such as \(+\),
    \(\times\), \(\wedge\), or \(<\), with their usual
    interpretation. 
    Recalling the motivating example, an example of a time condition is \(t' < t+ d\) for some temporal variables \(t,t',d \in \TX\).
    We also let a dedicated set of function symbols and constants \(\sig_e\), called \emph{events}. %, that do not appear in rewrite rules.

    \begin{definition}[Process]
      The grammar of \emph{processes} is: % the following grammar:
      \[\begin{array}{r@{\ }l@{\qquad}l}
        P ::= \quad
          & 0 & \text{\descrfont null process} \\
          & \NewP k; P & \text{\descrfont new name} \\
          % & \IfP\ u = v\ \LetInP\ P\ \ElseP\ P & \text{\descrfont conditional} \\
          & \InP[u]{x}\stamp{t}\ \WhenP\ b; P & \text{\descrfont timed input} \\
          & \OutP[u]{v}\stamp{t}\ \WhenP\ b; P & \text{\descrfont timed output} \\
          & \EventP\ \evfun(\vec{u})\stamp{t}\ \WhenP\ b; P & \text{\descrfont timed event} \\
          & P \mid P & \text{\descrfont parallel composition} \\
          & \BangP P & \text{\descrfont replication} \\
          & P + P & \text{\descrfont external choice}
      \end{array}\]
      where \(k \in \N\), \(u,v\) are terms, \(\vec{u}\) is a sequence terms (respecting the number of arguments of the event symbol \(\evfun[\vec{x}] \in \sig_e\)), \(x \in \X\), \(t \in \TX\), and \(b\) is a time condition (that may make reference to \(t\)).
      Naturally, for succinctness, the timestamp \(t\) and the time conditions \(b\) may be omitted if unused, thus subsuming the syntax of the usual applied \(\pi\) calculus for untimed processes.
      % We also omit \(0\) at the end of processes. 
      % For instance, \(\InP{u}{x}\) is shorthand\(\InP{u}{x}\stamp{t}\ \WhenP\ \truefun \; 0\).
    \end{definition}

    % Intuitively, \(0\) models a terminated process and all final ``\(; 0\)'' are usually omitted for readability.
    Intuitively, \(\NewP k\) binds a name \(k\), typically modelling a fresh session nonce.
    An instruction \(\alpha \stamp{t}\ \WhenP\ b; P\) executes \(\alpha\), provided \(b\) is satisfied, and records the current time inside \(t\).
    More specifically, the instructions \(\InP[u]{x}; P\) and
    \(\OutP[u]{v}; P\) model, respectively, inputs and outputs on a
    communication channel \(u\).
    As the adversary is assumed to control the communication network, in case \(u\) is public (e.g., \(u \in \sig_0\)), an output on \(u\) adds \(v\) to the adversary's knowledge while an input \(x\) is crafted by the adversary, possibly computing a new message from previous outputs.
    In case \(u\) is private (e.g., \(u \in \N\)), the communication is done synchronously (\emph{internal communication}), without adversarial interferences.
    Most often, we will however use the simpler notations:
    \begin{align*}
      \InP{x}; P & & \OutP{v}; P
    \end{align*}
    referring to an implicit arbitrary public channel.
    Finally, \(\EventP\ \evfun(\vec{u})\) is a meta instruction used to formalise security properties.
    % 
    Regarding structural operators, \(P \mid Q\) models two processes
    executed concurrently.
    Its unbounded analogue \(\BangP P\) represents infinitely many parallel copies of \(P\).
    % in particular \(\BangP \NewP k; P\) models an unbounded number of parallel sessions of the protocol \(P\), each with its own session nonce \(k\).
    The \emph{external} non-deterministic choice \(P+Q\) is a classical extension of the calculus that executes either \(P\) or \(Q\).
    The branching is only effective to actually execute an instruction from \(P\) or \(Q\), thus preventing to branch to a stuck process; 
    this makes it more suited for modelling liveness than some of its variants~\cite{BDK17}.

    \paragraph{Syntax Extensions}
    One may note that our grammar does not provide a syntax for ``\(\IfP\ u = v\ \ThenP\ P\ \ElseP\ Q\)'' to perform tests as in the motivating example.
    It can however be encoded as the process
    \[(\EventP\ \posfun(u,v); P) + (\EventP\ \negfun(u,v); Q)\]
    and by enforcing within security properties (see Section~\ref{sec:time-security}) that \(\posfun(u,v)\) always implies that \(u\) and \(v\) have the same normal form, and \(\negfun(u,v)\) implies that they do not.
    Such encodings of tests are standard in practical analysers, see, e.g.,~\cite{KK16}, and we will therefore use the ``\(\IfP \ldots \ThenP \ldots \ElseP\)'' syntactic sugar for convenience.
    Another convenient syntax extension is the \(\LetP\) binding \(\LetP\ x = u\ \LetInP\ P\) that evaluates the term \(u\), and binds its normal form to \(x\) in \(P\).
    This may be crucial, for example, to precompute the result of a timed primitive that will be used several times in \(P\).
    It is encodable using internal communications, i.e., given a fresh name \(e \in \N\):
    \[\OutP[e]{u}; 0 \mid \InP[e]{x}; P\]

  \subsection{Adversaries}
    We consider an adversarial semantics where processes are executed
    in parallel with an adversary that impersonates corrupted parties and builds knowledge from observing the values output by processes.
    We define in this section the related notions (adapted to the timed setting).
    First of all, we introduce a form of store recording the computations performed during the execution---including the adversarial knowledge.
    Formally, a \emph{dated frame} is a substitution:
    \begin{align*}
      \Phi & = \{x_1\stamp{t_1} \mapsto u_1, \ldots, x_n\stamp{t_n} \mapsto u_n\}
    \end{align*}
    where an entry \(x_i\stamp{t_i} \mapsto u_i\) models a term \(u_i\) computed at time \(t_i \in \setR[+]\) and stored under the handle \(x_i \in \X\). 
    This induces a notion
    % \[\xi \tded[\Phi]{t} u\]
    of \emph{dated computability},
    where \(\xi,u\) are terms and \(\Phi\) is a dated frame, defined by the inference rules of Figure~\ref{fig:time-deduce}.
    Intuitively, \(\xi \tded[\Phi]{t} u\) indicates that, by time
    \(t\), the evaluation of \(\xi \Phi\) has resulted in
    \(u\). %, potentially using parallel processors.

    \begin{myexample}
      Consider the following dated frame giving access to a commitment and its opening data \(m,r \in \N\):
      \begin{align*}
        \Phi & = \{
          x_1\stamp{t_1} \mapsto \commit(m,r,d),\ 
          x_2\stamp{t_2} \mapsto m,\ 
          x_3\stamp{t_3} \mapsto r\}
      \end{align*}
      The commitment may be opened using the data revealed at time \(t_2,t_3\), or forced starting at time \(t_1\) and for a duration of \(d\).
      Formally:
      \begin{align*}
        \open(x_1,x_2,x_3) & \tded[\Phi]{\max(t_1,t_2,t_3)} \okfun &
        \force(x_1) & \tded[\Phi]{t_1+d} m \qedhere
      \end{align*}
    \end{myexample}


    The adversarial computations themselves (with access to a dated frame) are then described by means of \emph{recipes}.

    \begin{definition}[Recipe] \label{def:recipe}
      We let an infinite set \(\AX \subseteq \X\) of dedicated variables called \emph{axioms}, not used in processes but possibly in a frame's domain.
      A \emph{recipe} is then a term \(\xi \in \termset(\sig \cup \sig_0 \cup \setR[+] \cup \AX)\).
      % A dated frame \(\Phi\) with \(\dom(\Phi) \subseteq \AX\) is specifically called a \emph{dated frame}.
    \end{definition}

    The axioms serve as handles to the protocols outputs, that the adversary reads by spying on the network.
    Thus, recipes describe how the adversary computes a message from the observations stored in a dated frame \(\Phi\).
    % This explains in particular why \(\ax \in \AX\) is called an \emph{axiom}:
    % outputs increase the attacker's knowledge as they aggregate, and are in some sense the axioms in the attacker's deduction proofs.
    Yet recipes may only use axioms, and not arbitrary variables, as the latter identify internal computations of the process that the adversary has not access to.
    Names are prohibited in recipes since they model secret values.

  \subsection{Operational Semantics}

    \input{fig_semantics}

    The actual operational semantics of the calculus operates on an extended form of process recording time and attacker's knowledge.

    \begin{definition}[Extended process]
      An \emph{extended process} is a tuple \((\P,\Phi,\gtime{t})\), with \(\P\) multiset of processes, \(\Phi\) dated frame, and \(\gtime{t} \in \setR[+]\).
      We often interpret a process \(P\) as the extended process
      \((\multi{P},\emptyset,0)\,.\)
    \end{definition}

    Intuitively, \(\P\) is the multiset of subprocesses currently being executed in parallel.
    The concrete time value \(\gtime{t}\) indicates the time elapsed since the beginning of the execution, while \(\Phi\) records the adversary's knowledge and the values computed during the protocol.
    %
    The semantics is then a labelled transition relation \(\cstep {\alpha}\), where \(\alpha\) is called an \emph{action} that may be either of:

    \begin{enumerate}
      \item an \emph{input action} \(\InP[\xi]{\zeta}\) modelling the reception of a message forged by the adversary, where
      \(\xi,\zeta\) are recipes;
      % proving that the adversary knows the communication channel where the message is expected, and \(\zeta\) is a recipe indicating how they computed the term to be input;
      \item an \emph{output action} \(\OutP[\xi]{\ax}\) modelling a message sent on the network, and recorded in the frame under axiom \(\ax\);
      % The recipe \(\xi\) indicates again how the adversary knows the channel;
      \item a non observable action, namely either \emph{silent action} \(\silent\), or an \emph{event action} \(\evfun(\vec{u})\), \(\vec{u}\) sequence of terms in normal form.
    \end{enumerate}

    We comment the relation, formalised in Figure~\ref{fig:semantics-time}.
    Rules~\ruletimein and \ruletimeout tackle public communications:
    outputs increase the attacker's knowledge (i.e., add an axiom to \(\dom(\Phi)\)), and inputs are computed using previous outputs (i.e., through a recipe \(\zeta\)).
    Note that Rule~\ruletimein renames \(x\) as \(x'\)
    simply to avoid conflicts when storing the result in \(\Phi\).
    The communication in Rules~\ruletimein and \ruletimeout is public in the sense that the channel \(u\) is computable by the adversary, using recipe \(\xi\).
    On the contrary, no interactions with the adversary arise in \ruletimecomm.
    Rule~\ruletimeevent triggers a meta action,
    materialising some control points to formalise security properties.
    Observe in particular that in the semantics of all instructions \(\alpha\stamp{t}\ \WhenP\ b\), the time condition \(b\{t \mapsto t_0\}\) is required to hold, which is how we handle that the time variable \(t\) may appear in \(b\).
    %
    Finally, \ruletimepar and \ruletimerepl spawn parallel threads, while \ruletimechoice uses either of two processes for the next transition.
    % 
    Rule \ruletic then lets time elapse, structuring the temporal behaviour of the protocol.
    Process executions thus alternate between \ruletic rules (that make time progress) and other transitions (that require enough time to have elapsed).
    % In particular, we formalise protocol executions as follows:

    \begin{definition}[timed trace]
      A \emph{timed trace} \(T\) of an extended process \(A_0\) is an infinite sequence of transitions of Figure~\ref{fig:semantics-time}:
      \begin{align*}
        T & : A_0 \cstep{\alpha_1} A_1 \cstep{\alpha_2} A_2 \cstep{\alpha_3} \cdots
      \end{align*}
      Finite portions of \(T\) may be more succinctly written \(A_p \Cstep{w} A_q\) when the intermediary processes are unimportant, where \(w\) is the word \(\alpha_{p+1} \cdots \alpha_q\) with \(\silent\) removed.
      In addition, we require that \(T\) is \emph{temporally structured}, i.e., writing \(A_i = (\P_i,\Phi_i,\gtime{t}_i)\):
      \begin{enumerate}
        \item \emph{time elapses between different actions}: \(\forall i \in \setN,\, \gtime{t}_i < \gtime{t}_{i+2}\);
        \item \emph{time progresses}: the sequence \((\gtime{t}_i )_{i \in \setN}\) is not bounded.
      \end{enumerate}
    \end{definition}

    The restriction to temporally structured traces is motivated by practice.
    Typically, the time progressing assumption is key when expressing fairness towards \(B\) in the motivating example (``\(B\) can get always the result by forcing the commitment''), as the property makes reference to moments arbitrarily far in the future.
    This also permits to consistently define the following notion of suffix:

    \begin{definition}[trace suffix]
      Let \(T = A_0 \cstep{\alpha_1} A_1 \cstep{\alpha_2} \cdots\) be a (temporally structured) trace, with \(A_i = (\P_i,\Phi_i,\gtime{t}_i)\).
      The \emph{suffix of \(T\) at time \(t \in \setR[+]\)}, \(t \geqslant t_0\), is the trace written \(T\stamp{t}\) and defined as
      \[
        % T\stamp{t}\ =\ 
        T_{\mathit{padding}} \cdot (A_i \cstep{\alpha_{i+1}} A_{i+1} \cstep{\alpha_{i+2}} \cdots)
      \]
      where \(i = \min\{j \in \setN \mid t \leqslant \gtime{t}_j\}\), 
      and \(T_{\mathit{padding}}\) is an empty transition if \(t = \gtime{t}_i\), and the \ruletic transition \((\P_i,\Phi_i,t) \cstep{} A_i\) otherwise.
    \end{definition}


    \begin{myexample}
      We refer to the motivating example of Section~\ref{sec:motivating} parametrised with \(d = 1\).
      Consider the timed trace \(T \cdot T_\infty\), where
      \[
        T : A(x,d) \Cstep{\OutP{\ax}\ \waitfun\ \InP{\xi}} (\multi{A'}, \Phi \cup \sigma, 0.59)
      \]
      and \(T_\infty\) is an infinite, temporally structured sequence of \ruletic transitions, \(\xi \tded[\Phi]{0.59} u\), and given fresh \(y',x_r \in \X, r' \in \N\):
      \[\begin{array}{r@{\ }l}
        A' & = \EventP\ \acceptfun(x \oplus y'); \OutP{x}; \OutP{x_r}; 0 \\
        \sigma & = \{x_r\stamp{0.09} \mapsto r', y'\stamp{0.59} \mapsto u\} \\
        \Phi & = \{\ax\stamp{0.1} \mapsto \commit(x,r',d)\}
      \end{array}\]
      In this trace, \(A\) initiates a session with the attacker \(B\) by sampling \(r'\) at time 0.09, commits on \(x\) with \(r'\) at time 0.1, and the attacker responds with a recipe \(\xi\) at time 0.59---not long before the timeout expiring at time 0.6.
      % In particular, since a force opening costs 1 unit of time, the attacker is unable to compute \(\force(\ax)\) at this point.
      However, if the timeout were \(d\) or a greater value (say, 1.1 here), the attacker could make the outcome be a chosen boolean \(b_0\) through another trace with the actions:
      \begin{itemize}
        \item \(\OutP{\ax_1}\) at time \(\gtime{t} = {0.05}\),
        \item \(\waitfun\) at time \(\gtime{t} = {0.06}\), 
        \item \(\InP{\force(\ax_1) \oplus b_0}\) at time \(\gtime{t} = {1.05}\),
        \item \(\acceptfun(b_0)\) at time \(\gtime{t} = {2}\). \qedhere
      \end{itemize}
    \end{myexample}