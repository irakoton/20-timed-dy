\section{Related Work}\label{sec:rw}

    \paragraph{Timed Cryptographic Primitives}
      % \gb{this paragraph can go to save space}
      Time-lock puzzles are the timed equivalent of an encryption scheme and have been introduced in the work of Rivest et al.~\cite{RSW96}. 
      This primitive has recently received a lot of attention, exploring constructions from new assumptions~\cite{TLPfromRE} or with new structural properties, such as homomorphism~\cite{MT19} or identity-based key derivation~\cite{Delay}. 
      The security of this primitive has been recently extended to CCA-security~\cite{CCATLP}, non-malleability~\cite{NMTLP}, and UC-security~\cite{UCTLP}. 
      Timed commitment is a related primitive~\cite{BN00,GJ02}, recently extended to the setting of verifiable timed signatures~\cite{TBM20}. 
      Another related notion is the recently introduced verifiable delay functions~\cite{BBB18}, that cannot encrypt messages but allow for efficient verification of the computation.


    \paragraph{Timed Symbolic Models}
      There exist several timed models to reason about protocols~\cite{BCS11,DD19,AEM20}. However, these models aim to show the physical proximity between participants (\emph{distance bounding}), by reasoning about network delays. These models do not consider timed cryptographic primitives, and are not supported by hyperlogics.
      One may still mention \cite{MST18}, that proves properties for distance-bounding protocols in \tamarin, recalling our implementation in Section~\ref{sec:implem}.
      However, in their (simpler) setting not involving timed cryptography, they can characterise time conditions statically, without the need to go over a refinement loop as us. 
      % 
      An example closer to ours may be~\cite{LSL15}, proposing a modelling of timed protocols, based on MSR like \tamarin.
      The security properties that it can express are however too limited (a finite set of trace properties).

    \paragraph{Fairness in Symbolic Models}
      Fairness has been intensively studied in the context of security protocols~\cite{KR01,KR02,SM02,GR05,CKS06}.
      These works often characterise it similarly as us, i.e., as a formula in a temporal logics similar to \ctl.
      They however build on finite models, which can mean considering only one or two protocol sessions, and/or approximating the power of the adversary by bounding the size of messages, thus often missing attacks.
      Some more recent work~\cite{ACC09,BDK17} lifts these restrictions by formalising fairness in a variant of the applied \(\pi\)-calculus, which is closer to our contributions.
      They are however limited to properties written \(\forall \pi, \progress \Rightarrow \varphi\) in our logic, and to protocols involving a TTP (i.e., no timed cryptography).
      Such applications can naturally be expressed in our framework.
      
    \paragraph{Hyperproperties}
      Hyperproperties~\cite{CS10a} is a unified framework encompassing classic trace properties, such as safety and liveness, relational properties, including equivalence relations commonly used to model security, and $k$-properties, which relate $k$ traces of a system. 
      Reasoning about hyperproperties therefore requires rich logics that are able to reason about multiple executions. 
      Such \emph{hyperlogics} include \ctl[Hyper]~\cite{CFK14} or its real-time analogue \mitl[Hyper]~\cite{HZJ20}, but also many others~\cite{CFH19}.
      Although our logic is inspired from them, there are major differences.
      First, we have an explicit model of cryptographic primitives, that can be referenced (through terms) in formulae.
      Second, we express properties about processes executed in an adversarial environment, and formulas can quantify over arbitrary adversarial computations.
      In particular, deciding whether an atomic formula holds is undecidable in our context without additional assumption---whereas it is a simple boolean test in usual hyperlogics---highlighting a gap in complexity.
      % 
      These logics are supported by a strong theoretical understanding of the limits of the problem in terms of decidability~\cite{FRS15,AFH96,FH16,MZ19,HZJ20}.
      This includes among others results of decidability for relational properties, including fragments of \ltl[Hyper], despite not carrying in general to \tidy{} variants as shown in Section~\ref{sec:decidability}. %, it would be interesting to study whether some results may carry to the \tidy{} variants. 
      % 
      There also exist formalisations of indistinguishability in concurrent process algebras with time, a few examples including~\cite{BKN12,SG13,NTU18}.
      They however only support a fixed set of cryptographic primitives, or not at all (pure \(\pi\)-calculus), and cannot express more refined variants of indistinguishability or hyperproperties in general.
      % It would in particular be insufficient to model any of our case studies.

      % \gb{equivalence of timed processes---we may want to mention there is a lot of work? Ugo? and cite a few ones, also NTU}
      % Still, there exist a few \ltl-based models of security properties in presence of an active adversary, for example~\cite{DGF07,LXY10,AMN13} that all include a model of time.
      % However these three models are unable to model indistinguishability properties or hyperproperties in general.
      % Their presentation is also tailored to specific crypto primitives (typically encryption) whereas our model is parametric in them.
      % We can also cite~\cite{ACC09}, that has similar features and restrictions, and provides a contribution similar to a work we cited previously~\cite{BDK17};
      % that is, it is a method for proving fairness in presence of a Dolev-Yao adversary, modelled as a trace property over progressing traces.
      % % 
      % One of the few models we found allowing to specify indistinguishability properties in a timed setting similar to ours is~\cite{NTU18}.
      % It considers the notion of what we call \emph{trace equivalence} in our paper.
      % Their framework does however not support arbitrary parallel composition of processes, only a fixed set of cryptographic primitives.
      % It would in particular be insufficient to model any of our case studies.