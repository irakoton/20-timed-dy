\section{Blockchain-Based Atomic Swap} \label{app:blockchain}

  We model in this section an additional protocol to complement the examples of Section~\ref{sec:protocols}.
  Although not involving timed cryptography explicitly, its timing behaviour is crisply characterisable by the constrained version of the operator \(\ltlfinally[I]\) (see Section~\ref{sec:extensions}), due to assumptions on the reactivity of participants.
  %
  The protocol is a folklore approach to perform atomic swaps of coins in blockchain-based cryptocurrencies~\cite{H18,MM18}. 
  For this overview, it is sufficient to think of a blockchain as an immutable append-only ledger where a user is associated with an account and can transfer coins to others. 
  In addition, it users \(A,B\) to establish contracts of the form:

  \begin{quote}
    \itshape \(\HTLC(\coinfun, y, d, B, A)\): if \(B\) posts a valid \(x\) such that \(y = \hfun(x)\) before time \(d\), then transfer the coin \(\coinfun\) to \(B\). 
    Otherwise transfer the coin to \(A\).
  \end{quote} 

  \noindent Here, \(\hfun\) is a cryptographic hash function and \(\HTLC\) stands for \emph{Hash Time-Lock Contract}
  (the term of ``time lock'' is however unrelated to the notion of time puzzle studied in this paper).
  % The intuition behind a time-lock contract is that the coin initially belongs to \(A\) who wants to buy something to \(B\).
  % The contract allows the transfer to occur, but refunds \(A\) if \(B\) fails to provide the bought product before the time limit \(2d\).
  With this functionality in mind, we describe how \(A\) and \(B\) can swap coins fairly.
  \begin{enumerate}
    \item \(A\) samples a random string \(r\), computes \(s = \hfun(r)\), posts on the blockchain a contract \(\HTLC(\coinfun_A, s, 2d, B, A)\), where \(d\) is some conservatively chosen time bound (e.g., a day).
    \item Once \(B\) verifies that \(A\) has posted a contract on the blockchain, they post a contract \(\HTLC(\coinfun_B, s, d, A, B)\) for the same \(s\) and half the duration of \(A\)'s time, i.e., \(d\).
    \item Since \(A\) knows a valid pre-image \(r\), it can redeem \(B\)'s coin by posting \(r\) on the blockchain before time \(d\). 
    If \(A\) does not act, the coin will return to \(B\) after time \(d\).
    \item Once \(r\) is available on the blockchain, \(B\) can redeem \(A\)'s coin too. 
    Even if \(A\) waits until the very end of the duration of \(B\)'s contract to act, \(B\) still has time \(2d - d = d\) to redeem \(A\)'s coin.
  \end{enumerate}

  To model the blockchain in our framework, we abstract it by a set of events  without apparent structure.
  We will then consider a security property of the form \(\forall \pi, H \Rightarrow \varphi\) where the formula \(H\) axiomatises the expected properties of a public ledger.
  This approach recalls symbolic models of similar protocols~\cite{BK19}, although one could argue that our framework, by using explicit time, makes some security implications more natural to express.
  Typically, the above protocol is unfair to \(B\) if \(B\) uses a contract of duration \(2d\) instead of \(d\), and more complex time-relevant relations arise for increasingly many parties~\cite{H18}.
  Concretely, we consider the theory:
  \[\begin{array}{lll}
    & \sigc = \HTLC/5, \hfun/1 
    & \sigd = \pi_i/1, i \in \{2,5\} \\
    % & \Equals(x,x) \to \okfun 
    & \pi_i(\HTLC(x_1,x_2,x_3,x_4,x_5)) \to x_i
  \end{array}\]

  The actual processes are then defined below, given \(\id_A,\id_B \in \sig_0\) modelling identities.
  For succinctness, we abstract a time parameter \(d\) from the process \(A\), but it could be encoded by an input of a temporal variable as in the multiple examples of Section~\ref{sec:protocols}.
  \[\begin{array}{r@{\ }l}
    A =\ 
    & \NewP r;\ \NewP \coinfun_A;\\ 
    & \LetP\ x = \HTLC(\coinfun_A,\hfun(r),2d,\id_A,\id_B)\ \LetInP \\
    & \OutP{x};\ \EventP\ \postfun(x, \id_A);\\
    & \InP{\gamma};\ \EventP\ \readfun(\gamma,\id_A);\\ 
    & \IfP\ \hfun(r) = \pi_2(\gamma)\ \ThenP \\
    & \IfP\ \id_A = \pi_5(\gamma)\ \ThenP \\
    & \EventP\ \postfun(r,\id_A); 0 \\[2mm] %\stamp{t'}\ \WhenP\ t' < t +\textstyle\frac{d}{2}
    % 
    B =\ 
    & \NewP \coinfun_B;\ \InP{\gamma}; \EventP\, \readfun(\gamma);\\
    & \LetP\ d = \pi_3(\gamma)\ \LetInP \\
    & \IfP\ \id_B = \pi_5(\gamma)\ \ThenP \\
    & \LetP\ x = \HTLC(\coinfun_B, \pi_2(\gamma), \textstyle\frac{d}{2}, \id_B, \id_A)\ \LetInP \\
    & \OutP{x};\ \EventP\, \postfun(x, \id_B); \\
    & \InP{r};\ \EventP\ \readfun(r,\id_B);\ \EventP\ \postfun(r,\id_B); 0\\[2mm]
    % 
    \transproc =\ 
    & \BangP \InP{x};\ \InP{\id_1};\ \InP{\id_2};\\
    & \phantom{\BangP} \EventP\ \givefun(x,\id_1,\id_2); 0 \\[2mm]
    % 
    \postproc(\id) =\ 
    & \BangP \InP{x};\ \EventP\ \postfun(x,\id); 0
  \end{array}\]
  The events \(\postfun(x,\id)\) indicate that \(x\) is posted by the participant of identity \(\id\) on the blockchain.
  When it is posted by the processes \(A\) or \(B\) (modelling honest agents), a public output is performed as well, modelling the attacker getting access to the ledger.
  The adversary, on the contrary, has access to the process \(\postproc(\id)\) as a form of posting oracle.
  The event \(\readfun(x,\id)\) then indicates that an arbitrary value on the blockchain is read by agent of identity \(\id\) (and we will restrict the analysis to traces such that \(\readfun\) events recover the desired posts).
  Finally, the process \(\transproc\) simply emits events expressing that some coin \(x\) is given from one agent to another and, again, the security property will restrain its behaviours.
  Formally, fairness for \(A\) (the formalisation for \(B\) is symmetric) can be modelled by the following formula, to be satisfied by the processes \(A \mid \transproc \mid \postproc(\id_B)\):
  \[\forall \pi.\, H_A \Rightarrow \ltlfinally \exists x,\id.\,\givefun(x,\id,\id_A)_\pi\]
  where \(H_A\) is the conjunction of the formulas below, modelling structural hypotheses on the ledger when \(A\) is honest.
  It typically expresses the properties of time-lock contracts, and that when \(B\) appends something on the blockchain, \(A\) will eventually read it, and reply, reasonably fast.
  To this end, we will use this time the interval-constrained temporal operator \(\ltlfinally[I]\), with the interval \(I = (0,\frac{d}{4})\) (we refer to Section~\ref{sec:extensions} for details).
  % We also abstract the parameter \(d\) from the formulas for succinctness.
  \begin{align}
    & \ltlglobally (\forall x.\, \postfun(x,\id_B)_\pi \Rightarrow \ltlfinally[I] \readfun(x,\id_A)_\pi) \\[2mm]
    % 
    & \begin{array}{@{}l@{}}
      \ltlglobally (\forall x.\, \readfun(x,\id_A)_\pi \Rightarrow
      \ltlfinally[I] (\stuck \vee \exists y, \postfun(y,\id_A)_\pi))
    \end{array} \\[2mm]
    % 
    & \begin{array}{@{}l@{}}
      \ltlglobally (\forall x, r, d, \id_1,\id_2.\\
      \qquad \postfun(\HTLC(x,\hfun(r),d,\id_1,\id_2))_\pi \Rightarrow \varphi \wedge \psi)
    \end{array} \\[2mm]
    % 
    \nonumber
    & \text{with}\ \varphi = \ltlglobally[(0,d)] (\postfun(r,\id_2) _\pi \Rightarrow \ltlfinally \givefun(x,\id_1,\id_2)_\pi) \\
    \nonumber
    & \text{and}\ \psi = (\ltlglobally[(0,d)] \neg \postfun(r,\id_2)_\pi) \Rightarrow \ltlfinally \givefun(x,\id_1,\id_1)_\pi
  \end{align}
