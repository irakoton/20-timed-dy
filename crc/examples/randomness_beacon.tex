\subsection{Randomness Beacon} \label{sec:beacon}
  To sample strings publicly, 
  % (in the context of a lottery for example), 
  one may apply an extraction function to entropy sources like stock markets~\cite{CH10} or blockchains~\cite{BCG15}.
  These sources are believed to produce high entropy but are also manipulable to some extent.
  To make such manipulations virtually useless, one may use a VDF (see
  Section~\ref{sec:string-sample}) as an extractor~\cite{BBB18}. 
  If its computation takes longer than the time necessary to influence the entropy source, an active attacker might indeed bias the source, but without  actual insight on the impact of their action.

  Concretely, consider, given an initial seed \(s\), a \emph{randomness beacon}~\cite{BBB18} that publishes a new random string at regular time intervals (say \(d \in \setR[+]\)). 
  It is required that the successive published strings remain unpredictable across the successive rounds, meaning:
  \begin{quote}
    \itshape \(n\)-round unpredictability: 
    given the transcript of the first \(n\) sampled strings, one cannot distinguish (in time less than \(d\)) the last string from a random one.
  \end{quote}
  Weaker variants may also be considered, based on computability instead of indistinguishability from random 
  (``the adversary should not be able to guess the actual value of the \(n\)\textsuperscript{th} string''), similarly to how we formalise unpredictability in the string-sampling protocol of Section~\ref{sec:string-sample}.
  % 
  The actual protocol simply proceeds iteratively by taking the last emitted string \(s\) (initially, the seed), and computes a new string \(r\) as the output of the VDF on input \(s\) with time parameter \(d\).
  It then reveals \(r\) (and the corresponding proof), sets \(s = r\), and repeats the process.
  We model unpredictability using the following two processes \(A_i\), \(i \in \{0,1\}\), given a private channel \(e \in \N\) to carry out the state of the beacon across multiple rounds, and \(d \in \TX\).
  \[\begin{array}{r@{\ }l}
    A_i =\ 
      & \InP{d}; (\initproc \mid \challengeproc_i(d) \mid \BangP \roundproc(d)) \\[1mm]
    % 
    \initproc =\  
      & \NewP s;\ \OutP[e]{s};\ \EventP\ \revealfun;\ \OutP{s}; 0 \\
    % 
    \roundproc(d) =\ 
    & \InP[e]{\statefun};\ \LetP\ r = \VDF(\statefun,d)\ \LetInP \\
    & \OutP[e]{r};\ \EventP\ \revealfun; \OutP{r}; 0\\
    % & \ElseP\ 0 \\
    % 
    \challengeproc_i(d) =\ 
    & \InP[e]{\statefun};\\
    & \NewP r_0;\ \LetP\ r_1 = \VDF(\statefun,d)\ \LetInP \\
    & \EventP\ \challengefun \stamp{t}; \OutP{r_i}; \\
    & \EventP\ \stopfun \stamp{t'}\ \WhenP\ t' < t+d; 0
    % & \ElseP\ 0
  \end{array}\]

  The process \(\initproc\) initialises the seed, \(\roundproc\) carries out one round of the generation, and \(\challengeproc_i\) reveals a string \(r_i\) and challenges to the adversary to guess, before the event \(\stopfun\), whether it is the last pseudo-random string \(r_1\), or a real random string \(r_0\).
  The process however allow the successive random strings \(r\) to be revealed gradually, whereas the security game requires them to be released in one go, after the challenge is issued.
  We enforce this property through what can be seen as \emph{trace restriction} \(H\), similarly to the feature of verification tools such as \proverif or \tamarin~\cite{manual-proverif,manual-tamarin}.
  Concretely, for all \(i \in \{0,1\}\), the following formula should be satisfied:
  \[\forall \pi \colon\! A_i.\, H_{\pi} \Rightarrow \exists \pi' \colon\! A_{1-i}.\, H_{\pi'} \wedge (\pi \StatEq \pi' \ltluntil \stopfun_\pi)\]
  where 
  \(H_\pi = \ltlfinally \stopfun_\pi \wedge (\neg \revealfun_\pi \ltluntil \challengefun_\pi)\).
