Remaining tasks
===

Rebuttal:
  - adapt the theory of commitments in all examples + benchmarks!
  - correct the d/2 everywhere
  - use . instead of , in quantifiers (appendices in particular)
  - standpoints regarding benchmarks

Proof of work modelling:
  - check what existing analyses do (e.g. Alexei IMDEA in Coq? Seems to be an "everyone honest" assumption)
  - how to limit the adversary's power?
  - goal: prove collusion bounds in consensus protocols?