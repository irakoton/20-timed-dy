\input{fig_semantics_symbolic}
\section{Reduction to Constraint Solving} \label{app:reduction}

  We give in this appendix the proof of Theorem~\ref{thm:constr-reduce}, that establishes the general decidability of the \verif problem provided constraint solving is decidable.
  The outline of the proof is the following:
  \begin{enumerate}
    \item we introduce a \emph{symbolic semantics} for our calculus (strongly inspired by analogue semantics in the untimed case~\cite{CCD13,CKR18}).
    It has the property of being finitely branching, i.e., bounded processes have a finite number of symbolic traces;
    \item we formalise its \emph{soundness} (``all symbolic traces yield a regular trace when the constraints they contain are satisfied'') and \emph{completeness} (``all traces can be abstracted by a symbolic trace'');
    \item we finally give a \emph{decision procedure} based on all of these properties.
    Intuitively, the symbolic semantics allows to reduce the path quantifications \(\forall \pi\) to the enumeration of a finite number of symbolic traces, while other quantifiers are translated directly inside the constraint.
  \end{enumerate} 

  \subsection{A Symbolic Semantics for Finite Traces}
    
    % We first define the aforementioned symbolic semantics.
    An \emph{abstract extended process} \(A\) is an extended process that may contain contain free variables (i.e., variables not bound by a prior input instruction) and free temporal variables where a real number is expected (typically, in the global time \(\gtime{t}\) in \(A = (\P,\Phi,\gtime{t})\), in time conditions \(b\), or in the domain of frames \(x \stamp{t}\)).
    We write \(\vars(A)\) the set of free variables of an abstract extended process \(A\).
    In particular, given a substitution \(\sigma\) with \(\vars(A) \subseteq \dom(\sigma)\), \(A\sigma\) is a regular extended process.
    % 
    The symbolic semantics itself then operates on \emph{symbolic processes} \(A,\Gamma\), \(A\) abstract extended process and \(\Gamma\) a conjunction of of atomic constraints.
    In particular, \(\Gamma\) does not contain quantifiers but may contain free variables, that are implicitly existentially quantified.
    The semantics itself then takes the form of a labelled transition relation \(\sstep{\alpha}\) where \(\alpha\) is a \emph{symbolic action}, that may be either:
    \begin{itemize}
      \item a symbolic input or output actions \(\InP[X]{Y}, \OutP[X]{\ax}\) where \(X,Y \in \X[2]\) are second order variables,
      \item a silent action \(\silent\), or an event \(\evfun(\vec{x})\), where \(\vec{x}\) are regular variables.
    \end{itemize}
    The relation \(\sstep{\alpha}\) is defined in Figure~\ref{fig:semantics-symbolic}, and essentially follows the same intuition as the regular semantics, except that the condition of rule applications are replaced by constraints stored in \(\Gamma\).
    We also use a notation \(\Sstep{w}\) analogue to the regular semantics.

    % \input{fig_semantics_symbolic}

    \begin{definition}[Symbolic trace]
      A \emph{symbolic trace} is a finite sequence of transitions \(A_0 \sstep{\alpha_1} \cdots \sstep{\alpha_n} A_n\), such that:
      \begin{itemize}
        \item either all even transitions (and exactly these) \(A_{2i-1} \sstep{\alpha_{2i}} A_{2i}\) are \sruletic transitions;
        \item or all odd transitions (and exactly these) \(A_{2i} \sstep{\alpha_{2i+1}} A_{2i+1}\) are \sruletic transitions.
      \end{itemize} 
    \end{definition}
    
    Note that a bounded process \(P\) has a finite number of symbolic traces, more precisely a number that is exponential in the size of \(P\).
    The finiteness is in particular due to the determinism of Rule~\sruletic, and the impossibility to chain multiple instances of this rule without interleaving another rule making the size of the process decrease.
    We now state the soundness and completeness of the symbolic semantics.
    It is however only valid for \emph{almost finite} traces, i.e., traces of the regular semantics \(A_0 \cstep{\alpha_1} A_1 \cstep{\alpha_2} \cdots\) such that there exists \(i \in \setN\) such that all transitions \(A_j \cstep{\alpha_{j+1}} A_{j+1}\), \(j \geqslant i\) are \ruletic transitions.
    Note that all traces of bounded processes are almost finite in the above sense.
    % In order to formalise the statement, we introduce the following notion:

    \begin{definition}[Solution]
      Let \(\Gamma\) be a constraint with free variables, and \(\Sigma,\sigma\) be two substitutions where \(\Sigma\) maps the free second order variables of \(\Gamma\) to recipes, and \(\sigma\) maps its regular and temporal variables to terms and non-negative real numbers, respectively.
      We say that \((\Sigma,\sigma)\) is a \emph{solution} of \(\Gamma\) if \(\Gamma\Sigma\sigma\) is valid.
      We write \(\Sol(\Gamma)\) the set of solutions of \(\Gamma\).
    \end{definition}

    Soundness itself then states that all symbolic traces yield concrete traces when instantiating free variables w.r.t. substitution satisfying the collected constraints.

    \begin{proposition}[Soundness] \label{prop:soundness}
      Let \(A,\Gamma \Sstep{w} A',\Gamma'\) be a symbolic trace and \((\Sigma,\sigma) \in \Sol(\Gamma')\).
      Then for all infinite, temporally structured sequence of \ruletic transitions \(T_\infty\), \((A\sigma \Cstep{w\Sigma\sigma} A'\sigma) \cdot T_\infty\) is a timed trace.
    \end{proposition}

    Second, completeness states that all concrete traces can be seen as the instance of a symbolic trace.

    \begin{proposition}[Completeness] \label{prop:completeness}
      Let \(A,\Gamma\) be a symbolic process and \((\Sigma,\sigma) \in \Sol(\Gamma)\).
      If \((A\sigma \Cstep{w} A') \cdot T_\infty\) is a timed trace, then there exists a symbolic trace \(A,\Gamma \Sstep{w_s} A'_s,\Gamma'\) and \((\Sigma',\sigma') \in \Sol(\Gamma')\) such that \(\Sigma \subseteq \Sigma'\) (meaning that \(\Sigma'\) extends \(\Sigma\)), \(A'_s \sigma = A'\) and \(w_s \Sigma = w\).
    \end{proposition}

    They are proved by a quick induction on the length of traces.
    % Note in particular that, in the soundness proposition, the requirement that traces are temporally structured is enforced by the compulsory \sruletic transition interleaved between all other pairs of transitions.

  \subsection{Reduction To Constraint Validity}

    We now prove Theorem~\ref{thm:constr-reduce} that reduces \verif to the constraint validity problem.
    For that we define a function \(\computecstr\) that takes as arguments a mapping \(\Pi\) from path variables to symbolic traces (of bounded processes), \(\pi_0 \in \dom(\Pi)\) and a \pictl[Hyper] formula \(\varphi\), and computes a constraint \(\Gamma\) that is valid, intuitively, when instances of \(\Pi\) satisfy \(\varphi\).
    We first define a symbolic analogue of trace suffixes.

    \begin{definition}[symbolic suffix]
      Let \(T = A_0 \sstep{\alpha_1} \cdots \sstep{\alpha_n} A_n\) be a symbolic trace and \(t \in \TX\).
      We write \(A_i = (\P_i,\Phi_i,\gtime{t}_i),\Gamma_i\), and \(\bar{A}_i = (\P_i,\Phi_i,t),\Gamma_i\).
      A \emph{symbolic suffix of \(T\) timestamped at \(t\)} is a pair \(T',\Gamma'\), with \(T'\) a (possibly empty) symbolic trace of the form 
      \begin{align*}
        T' & : \bar{A}_i \sstep{\alpha_{i+1}} A_{i+1} \sstep{\alpha_{i+2}} \cdots \sstep{\alpha_n} A_n & i \in \eint{0}{n}
      \end{align*}
      and \(\Gamma'\) is the following constraint:
      \begin{itemize}
        \item if \(i = n\) then \(\Gamma' = \gtime{t}_i < t\);
        \item if \(i \in \eint{0}{n-1}\) and \(A_i \sstep{\alpha_{i+1}} A_{i+1}\) is a \sruletic transition, then \(\Gamma' = \gtime{t}_i < t < \gtime{t}_{i+1}\);
        \item if \(i \in \eint{1}{n-1}\) and \(A_i \sstep{\alpha_{i+1}} A_{i+1}\) is not a \sruletic transition, then \(\Gamma' = (t = \gtime{t}_i)\).
      \end{itemize}
    \end{definition}

    Note that there are finitely (polynomially) many symbolic suffixes.
    The definition of \(\computecstr\) is then given in Algorithm~\ref{alg:reduction}.
    
    \input{fig_algo}

    The desired property of the algorithm can be formalised by the proposition below.
    Let \(\Pi\) be a mapping from path variables to symbolic traces, with \(\dom(\Pi) = \{\pi_0,\pi_1, \ldots, \pi_n\}\) for some \(n \geqslant 0\), and assuming without loss of generality that all \(\Pi(\pi_i),\Pi(\pi_j)\) do not share free variables if \(i \neq j\).
    We then call \((\Sigma,\sigma)\) a \emph{solution} of \(\Pi\) when \(\Sigma = \Sigma_0 \uplus \cdots \uplus \Sigma_n\) and \(\sigma = \sigma_1 \uplus \cdots \sigma_n\) with \((\Sigma_i,\sigma_i) \in \Sol(\Gamma_i)\) and \(\Gamma_i\) the final constraint of the symbolic trace \(\Pi(\pi_i)\).
    We write \(\Sol(\Pi)\) the set of solutions of \(\Pi\).
    Below, we treat indistinctly almost finite traces and those that may become so when extended with an arbitrary temporally structured sequence of \ruletic transitions.

    \begin{proposition}
      Let \(\Pi\) be a mapping as above, \(\pi_0 \in \dom(\Pi)\) and \(\varphi\) be a \pictl[Hyper] formula, potentially with free variables \(\vars(\varphi)\), but path variables among them are in \(\dom(\Pi)\), and other variables do not appear in \(\im(\Pi)\).
      We also let \((\Sigma,\sigma) \in \Sol(\Pi)\), and \(\Sigma'\) (resp. \(\sigma'\)) a substitution mapping all second order (resp. regular) free variables of \(\varphi\) to recipes (resp. terms).
      Then \(\Pi(\pi_0)\Sigma\sigma, \Pi\Sigma\sigma \models \varphi\Sigma'\sigma'\) \textit{iff} \(\computecstr(\pi_0,\Pi,\varphi)\Sigma\Sigma'\sigma\sigma'\) is valid.
    \end{proposition}

    Note that, in the statement of the proposition, we implicitly use the soundness of the symbolic semantics (Proposition~\ref{prop:soundness}) when we consider \(\Pi(\pi_0)\Sigma\sigma\) as a valid trace.
    Besides, the assumption that non-path variables of \(\varphi\) do not appear in the symbolic traces is without loss of generality, and is here to ensure that the consecutive applications of substitutions \(\Sigma\Sigma'\sigma\sigma'\) are commutative.
    The proof of the property can then be found below.

    \begin{proof}
      We prove the property by induction on \(\varphi\).
      We thus tet \(\Pi\) be a mapping of non-empty domain \(\dom(\Pi)\) and whose image are symbolic traces of bounded processes.
      Let \(\pi_0 \in \dom(\Pi)\), and \(\varphi\) be a \pictl[Hyper] formula such that \(\vars(\varphi) \cap \vars(\im(\Pi)) = \emptyset\).
      We also let \((\Sigma,\sigma) \in \Sol(\Pi)\) and \((\Sigma',\sigma')\) arbitrary extensions with 
      \[\vars(\varphi) \smallsetminus \dom(\Pi) = \dom(\Sigma') \cup \dom(\sigma')\,.\]
      % If \(T\) is a finite sequence of transitions of the semantics, we write \(T_\infty\) the infinite extension of \(T\) with an infinite, temporally structured sequence of \ruletic transitions.
      % We will often refer in the proof to the soundness and completeness of the symbolic semantics (respectively, Propositions~\ref{prop:soundness} and \ref{prop:completeness}).
      
      \caseitem{Case \(\varphi\) atomic formula}
      We simply treat one case to give the proof structure, e.g., \(\varphi = \InP[\xi]{\zeta}_\pi\), and all other cases are either straightforward or following a similar approach.
      We still mention that the case \(\varphi = \evfun(\vec{u})\) relies on the convergence assumption on the rewriting system \(\R\), i.e., that normal forms are unique.

      By hypothesis, \(\pi \in \dom(\Pi)\), and we thus write \(\Pi(\pi) : A \sstep{\alpha} B \sstep{w} C\).
      If \(\alpha\) is not an input action, the result holds as neither \(\computecstr(\pi_0,\Pi,\varphi) = \bot\) is valid, nor \(\Pi(\pi_0)\Sigma\sigma, \Pi\Sigma\sigma \models \varphi\Sigma'\sigma'\).
      Otherwise let us write \(\alpha = \InP[X]{Y}\), and therefore 
      \begin{align*}
        \alpha\Sigma & = \InP[X\Sigma]{Y\Sigma} \\
        \computecstr(\pi_0,\Pi,\varphi)\Sigma\Sigma'\sigma\sigma' & = (X\Sigma = \xi\Sigma' \wedge Y\Sigma = \zeta\Sigma')
      \end{align*}
      We thus obtain the expected result.

      \caseitem{Case \(\varphi = \forall \omega.\, \psi\), with \(\omega\) non path variable, or \(\varphi = \psi_0 \wedge \psi_1\) or \(\varphi = \neg \psi\)}
      Straightforward reductions to the induction hypothesis(es).
      
      \caseitem{Case \(\varphi = \forall \pi.\, \psi\), with \(\pi\) path variable}
      We have to prove that \(\Pi(\pi_0)\Sigma\sigma, \Pi\Sigma\sigma \models \forall \pi.\, \psi\Sigma'\sigma'\) \textit{iff} the following constraint is valid:
      \[\bigwedge_{i=1}^p \forall \vec{\omega}_i.\, \Gamma_i \Rightarrow \computecstr(\pi,\Pi \cup \{\pi \mapsto T_i\},\psi)\]
      where \(\{T_1, \ldots, T_p\}\) is the set of symbolic traces of the first symbolic process of \(\Pi(\pi_0)\), \(\vec{\omega}_i\) is the set of fresh variables introduced by \(T_i\), and \(\Gamma_i\) is the final constraint of \(T_i\).
      Using the induction hypothesis applied to \(\psi\), the forward direction of this equivalence then simply follows from the soundness of the symbolic semantics,
      % (Proposition~\ref{prop:soundness}), 
      while the converse direction follows from completeness.
      %  (Proposition~\ref{prop:completeness}).

      \caseitem{Case \(\varphi = \psi_0 \ltluntil \psi_1\)}
      We refer to the notations of the corresponding case of Algorithm~\ref{alg:reduction} due to their heavy number.
      Let us assume \(\Pi(\pi_0)\Sigma\sigma, \Pi\Sigma\sigma \models \psi_0\Sigma'\sigma' \ltluntil \psi_1\Sigma'\sigma'\), and prove that 
      \[\computecstr(\pi,\Pi,\psi_0 \ltluntil \psi_1)\Sigma\Sigma'\sigma\sigma'\] 
      is valid.
      This is easily obtained by instantiating the variable \(t_0\) by the corresponding \(t+\delta \in \setR[+]\) given by the hypothesis on the satisfaction of \(\psi_0 \ltluntil \psi_1\).
      The converse implication is obtained in the same way, observing that the constraints generated by symbolic suffixes timestamped at \(t_0\) imply all constraints \(t_0 > t\), with \((\P,\Phi,t),\Gamma\) initial symbolic process of \(T_i\), \(i \in \eint{1}{n}\).
    \end{proof}

    \begin{corollary}[Correctness of the reduction]
      Let \(P\) be a bounded process and \(\varphi\) be a \pictl[Hyper] formula.
      We consider \(\Pi = \{\pi \mapsto \epsilon_P\}\) where \(\epsilon_P\) is the empty symbolic trace starting from \((\multi{P},\emptyset,0),\top\).
      Then \(P \models \varphi\) \textit{iff} \(\computecstr(\pi,\Pi,\varphi)\) is valid.
    \end{corollary}

    \clearpage\clearpage