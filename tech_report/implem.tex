% \section{Automated Verification of \texorpdfstring{\piltl}{tidyLTL}} \label{sec:implem}
\section{Automated Verification} \label{sec:implem}
  We conducted a preliminary evaluation of our timed model with \tamarin, a popular verification tool for untimed protocols~\cite{manual-tamarin}. %, to verify protocols of Section~\ref{sec:protocols}.
  Our approach is based on successive counterexample-guided refinements of an untimed model.
  % We describe our approach and results here.
  % 
  \tamarin supports the syntax of the applied \(\pi\)-calculus, but internally uses \emph{multiset rewrite rules} (MSR):
  \[[\ \vec{u}\ ]-\!\![\ \vec{v}\ ]\!\!\rightarrow [\ \vec{w}\ ]\]
  An execution state in \tamarin can be seen as a multiset of terms, and applying the above rule removes the terms \(\vec{u}\) from it, adds in addition the terms \(\vec{w}\), and labels the operation with the \emph{facts} \(\vec{v}\) (similarly to events in our model).
  Cryptographic primitives can be specified in a term algebra similar to the one we use.
  Regarding security properties, \tamarin supports among others a first-order logic with explicit timestamps (with a restriction similar to our notion of guarded formulae in Section~\ref{sec:lower}).
  This allows in particular to express most properties of interest built from the ``until'' operator.
  % 
  Given a \piltl formula \(\forall \pi.\, \varphi\) and a process \(P\) (not necessarily bounded), we used the following approach to (dis)prove \(P \models \forall \pi.\, \varphi\). % while relying \tamarin as a backend.
  \begin{enumerate}
    \item When a symbol \(\ffun[\vec{x}]\) has a non-null cost, we declare it as a \emph{private symbol} in \tamarin, meaning that the adversary cannot use it to build recipes.
    We then add a MSR:
    \[\tag{\mbox{\(\star\)}} \label{msr:time-app}
      [\ \InP{\vec{x}}\ ]-\!\![\ \mathsf{App\_f}(\vec{x})\ ]\!\!\rightarrow [\ \OutP{\ffun(\vec{x})}\ ]\]
    This allows the adversary to apply \(\ffun\) arbitrarily by using this rule as a form of oracle that outputs \(\ffun(\vec{x})\) upon reception of \(\vec{x}\).
    This way, all applications of \(\ffun\) are materialised in traces by a fact \(\mathsf{App\_f}\).
    For simplicity, we modelled all timed cryptography this way, i.e., by making without costs on rewrite rules.
    \item \label{it:implem-time-verif}
    We then erase time annotations from \(P\) (timestamps, time conditions).
    We record this erasure, syntactically, by adding fresh labels on the corresponding MSR (called \(\mathsf{Timestamp}\) facts in the following).
    This results in an untimed process \(\bar{P}\) whose set of traces \emph{supersedes} that of \(P\).
    We then verify \(\bar{P} \models \forall \pi.\, \varphi\) using \tamarin.
    Assuming the analysis terminates and conclusive:
    \begin{enumerate}[leftmargin=*]
      \item \label{it:implem-proof}
      either we get a proof;
      \item \label{it:implem-attack}
      or \tamarin returns a trace violating \(\varphi\), displayed as a DAG \((V,E)\).
      Its vertices are MSR instances 
      % (including \eqref{msr:time-app} and MSR for adversarial computations) 
      timestamped by temporal variables, and an edge \((r_1\stamp{t_1} \to r_2\stamp{t_2}) \in E\) can be seen here as a temporal dependency \(t_1 < t_2\).
    \end{enumerate}
  \end{enumerate}

  In case~\eqref{it:implem-proof}, we conclude that \(P \models \forall \pi.\, \varphi\).
  This is sound for \piltl formulae, but not in general in presence of arbitrary path quantifiers as in liveness or indistinguishability properties.
  This was however sufficient to cover several protocols among those presented in Section~\ref{sec:protocols}, provided we replace indistinguishability properties by weaker variants based on computability (\(\atk\) formula).

  In case~\eqref{it:implem-attack}, we verify that the attack graph is consistent with the erased time annotations (marked by \(\mathsf{Timestamp}\) facts) and the computation costs (marked by \(\mathsf{App\_f}\) facts when performed by the adversary).
  This amounts to a simple SMT solving of these time constraints and of the inequalities induced by the graph edges.
  For example, when analysing the motivating example, \tamarin finds a potential violation of fairness towards \(A\);
  that is, a way to obtain \(A\)'s commitment \(x\) before the \(\waitfun\) event.
  A simplified attack graph is informally pictured below, with time annotations added:
  % in Figure~\ref{fig:attack-graph}.
  
  \bigskip
  
  \begin{center}
    \begin{tikzpicture}
      [
        action/.style={draw,rectangle},
        proc/.style={},
        title/.style={draw,rectangle,fill=black!10}
      ]
      \node[action] (action1) {\(\msr_1 \stamp{t}\)};
      \node[proc] (invi1) [below of = action1, node distance = 8mm] {};
      \node[action] (action2) [right of = invi1, node distance = 35mm] {\(\msr_2 \stamp{s}\)};
      \node[proc] (descr2a) [right of = action2, node distance = 10mm] {\eqref{msr:time-app}};
      \node[proc] (invi2) [below of = action2, node distance = 8mm] {};
      \node[action] (action3) [left of = invi2, node distance = 35mm] {\(\msr_3 \stamp{t_s}\)};
  
      \draw[->] (action1) edge [bend left = 20] (action2);
      \draw[->] (action2) edge [bend left = 20] (action3);
      \draw[->] (action1) edge (action3);

      \node[proc] (descr1) [below of = action1, node distance = 5mm] {{\small \colorbox{white}{output of \(\omega = \commit(x,r,d)\)}}};
      \node[proc] (descr2) [below of = action2, node distance = 5mm] {{\small \colorbox{white}{Adv. computes \(\force(\omega) = x\)}}};
      \node[proc] (descr2a) [below of = action2, node distance = 8mm] {{\small \bfseries\sffamily\itshape cost annot.: \(s \geqslant t+d\)}};
      \node[proc] (descr3) [below of = action3, node distance = 5mm] {{\small \colorbox{white}{event \(\waitfun\)}}};
      \node[proc] (descr3a) [below of = action3, node distance = 8mm] {{\small \bfseries\sffamily\itshape erased annot.: \(t_s < t + d\)}};
    \end{tikzpicture}
    % \caption{Simplified attack graph returned by Tamarin against fairness towards \(A\) in the motivating example}
    % \label{fig:attack-graph}
  \end{center}
  % 
  If the overall induced time constraint is satisfiable by an instantiation of the timestamps as real numbers, we conclude that \(P \not\models \forall \pi.\, \varphi\).
  However, in the case of the above attack graph, the constraint is:
  \[(t < s < t_s) \wedge (s \geqslant t+d) \wedge (t_s < t+d)\]
  which cannot be satisfied by any instantiation of \(t,t_s,s\).
  This means that the attack graph does not correspond to a valid \emph{timed} trace, and we generate a formula \(\psi\) that excludes this inconsistent trace \(T\) exactly.
  It is a straightforward \tamarin formula stating that, if all of the facts in \(T\) occur, then at least one inequality induced by the edges of the graph of \(T\) should be false.
  We then add \(\psi\) as a \emph{trace restriction}, which intuitively means repeating the verification to prove \(\bar{P} \models \forall \pi.\, (\psi \Rightarrow \varphi)\).
  % 
  The results we obtained with this method when analysing protocols of Section~\ref{sec:protocols} are collected in Figure~\ref{fig:bench}.
  They indicate how many times \tamarin was additionally run in our iterative approach to eliminate invalid attacks at Step~\ref{it:implem-attack} of the procedure, and how many auxiliary lemmas were added manually to guide \tamarin's solver.
  All modelling files can be found online:
  \begin{center}
    \small \url{https://gitlab.com/irakoton/20-timed-dy/-/releases/tech-report}
  \end{center}
  
  \input{examples/bigtable}

  \paragraph{Discussion}
    We currently conducted the steps of our approach manually (model writing, test and exclusion of invalid attacks), 
    % mostly due to the difficulty of extracting \tamarin attack traces in a non-graphical format.
    although automating these steps would however only require a mostly non-technical engineering effort.
    % 
    The limitation to \piltl is more significant, excluding for example the contract signing protocol and its only liveness security property.
    % However, the underlying formula falls into the partial decidability results provided in Section~\ref{sec:decidability}.
    An important direction for future work is hence to study the decidability of constraint solving to verify examples outside the scope of our embedding in \tamarin, by relying on our results in Section~\ref{sec:upper}.
    This notably still remains a challenge even in the untimed case, although decision procedures for specific hyperproperties could be relevant starting points~\cite{CKR18}.
    One could also rely on the support of \tamarin for liveness~\cite{BDK17} and indistinguishability; 
    this may however require to discard our counterexample-guided approach in favour of a \emph{static} generation of trace restrictions characterising timed behaviours. %, in a sound and complete manner.

