\section{Introduction}

  Multi-party protocols are distributed programs executed by heterogeneous entities, with the underlying goal of computing an output, e.g., a signed contract, the result of an auction, or simply a random value. Although the output is intended to be shared by all participants, some parties may have an incentive to prevent other parties from obtaining it. Or, they may decide to abort the computation if the potential output is not of their convenience. To exemplify the issue, consider the prototypical scenario where two mutually distrustful parties \((A, B)\) want to agree on a common random string. 
  A natural protocol to achieve this goal would be to let \(A\) sample a random string \(x\) locally and then send a \emph{commitment} on \(x\) to \(B\).
  % For the purpose of this overview, one can think of a commitment simply as a perfectly sealed envelope. 
  After \(B\) responds with a uniformly sampled bit \(y\), \(A\) can then reveal the committed message and the output of the protocol is set to \(z = x \oplus y\). The rationale of this protocol is that \(y\) cannot depend on \(x\) (the commitment hides \(x\) to \(B\)), while at the same time \(x\) cannot be changed ``after the fact'' (the commitment binds \(A\) to \(x\)).
  
  Unfortunately, this simplistic analysis neglects to account for the fact that \(A\) learns the output of the protocol before \(B\). 
  Therefore, \(A\) may decide to stall the protocol, by not opening the commitment, if the output is not favourable. Using this strategy, \(A\) can bias the distribution of \(z\) arbitrarily. 
  The root of the problem is that this protocol does not satisfy \emph{fairness}, a cryptographic notion that guarantees that either all parties obtain the output of the protocol, or none of them does. 
  This property is notoriously hard to enforce, and it is in fact known to be unachievable in classic models of asynchronous communications~\cite{EY80,C86}. One way to circumvent this impossibility result is to assume a \emph{trusted third party} (TTP), responsible to deliver the output to all participants. However, the assumption of a TTP is unrealistic in many scenarios, raising the question of whether multi-party protocols can realise fairness without trusted parties. 
  
  % \gm{The above paragraphs may begin a little bit too much ``in medias res'', I am open to suggestions.}
  
  \paragraph{Fairness via Timed Cryptography}  Modern cryptography bypasses this barrier by relying on \emph{timed cryptographic primitives}, i.e., primitives that are assumed not to be computable in less than a chosen amount of time~\cite{RSW96}. As an example, \emph{timed} commitments~\cite{BN00} allow to hide a message for a pre-determined amount of time \(d\). Going back to our above problem of string sampling, timed commitments are the right tool to construct a fair string sampling protocol: \(A\) can no longer withhold the opening of its commitment, since it will anyway be public in time \(d\). On the other hand, the timed commitment still ensures that \(B\) does not learn the committed value during the protocol, as long as the protocol terminates in time less than \(d\). Importantly, both parties need to place a strict timeout on the duration of the protocol to ensure that this requirement is met.
  
  Other examples of timed cryptographic primitives include verifiable delay functions~\cite{BBB18}, verifiable timed signatures~\cite{TBM20}, and (homomorphic) time-lock puzzles~\cite{RSW96,MT19}. These primitives have been developed to enforce fairness at a cryptographic level in a variety of settings, and count numerous applications: sealed-bid auctions~\cite{BN00}, fair contract signing~\cite{BN00}, randomness beacons~\cite{BBB18}, computational time-stamping~\cite{BBB18}, or even electronic voting~\cite{MT19}.
  % self-decrypting files~\cite{self}, or prevention of front-running~\cite{CDN}, to mention a few.
  
  % \gm{Too much?}
  %REFERENCES ABOVE:
  %https://www.gwern.net/Self-decrypting-files
  %https://static1.squarespace.com/static/59aae5e9a803bb10bedeb03e/t/5e8397b1d7775927b4f9d497/1585682356053/clockwork.pdf
  
  \paragraph{Symbolic Analysis of Cryptographic Protocols} 
    Cryptographic protocols are complex and error-prone, but there exist symbolic models in which these protocols can be specified and verified. These models, such as the Dolev-Yao model~\cite{DY81} and the subsequent applied \(\pi\)-calculus~\cite{ABF17}, view cryptographic primitives as (idealised) algebraic constructions, and focus on the interactions of the protocol's participants. Thanks to this abstraction, security properties can be expressed as logical formulae and can be verified, expectedly automatically, by means of decision procedures or proof systems. The symbolic analysis of cryptographic protocols is a flourishing area of research, supported by performing tools that have been used to analyse or guide the design of widely deployed protocols~\cite{BBB21}.
    However, current symbolic models disregard low-level considerations such as the execution time and, as such, are not suitable to reason about protocols built on top of timed cryptography. 
    % Given the wide scope of these primitives, this leaves us in an unsatisfactory state of affairs. 
    % The goal of this work is to fill this gap.

  \subsection*{Contributions}
    In this paper, we propose the first symbolic model of cryptographic protocols built over timed cryptography.
    \begin{enumerate}
      \item We propose a model \emph{à la Dolev-Yao} of timed cryptography, and of timed distributed processes executed in an unbounded adversarial environment. 
      For that we extend the applied \(\pi\)-calculus~\cite{ABF17}, a classical framework for specifying cryptographic protocols, with time constraints.
      % 
      \item We introduce a logic for expressing security properties of such processes, inspired by popular logics for hyperproperties such as \ctl[Hyper]~\cite{CFK14,HZJ20}.
      We call ours \pictl[Hyper] as it is designed to express hyperproperties in \emph{timed Dolev-Yao} models.
      A major difference with existing hyperlogics---designed for finite-state systems---is our ability to quantify over unbounded (adversarial) computations. 
      This makes our framework substantially more expressive in the context of protocols, but also more complex from a decidability standpoint.
      % This includes, among others, \emph{reachability} properties such as event correspondences, \emph{liveness} properties such as timeliness, but also refined variants of \emph{indistinguishability} (e.g., ``some secret is indistinguishable from random until the security guarantee of a timed primitive expires'').
      % 
      \item We illustrate this gap through complexity lower bounds for the verification problem. 
      Typically, even the non-relational fragment of our logic, \pictl, is undecidable while the analogue problem in \ctl (i.e., model checking) is \pspace complete.
      On the positive side, we show that verifying that a bounded number of sessions of a protocol satisfy a \pictl[Hyper] formula can be reduced to a form of \emph{constraint solving}. 
      It is inspired by popular analogues in protocol analysis used to verify reachability and indistinguishability properties in the untimed case~\cite{CCD13,CKR18}.
      % 
      \item Finally, we verify timed safety properties (\piltl) by relying on \tamarin, a popular automated prover for (untimed) security protocols.
      We develop an approach to handle time constraints while relying on the tool as a backend soundly, and successfully analyse several properties of interest of various protocols.
    \end{enumerate}
