\section{Lower Bounds in the Pure Calculus} \label{app:low-pure}

  We prove in this section the complexity lower bounds for bounded processes of the pure \(\pi\) calculus, i.e., when the signature is empty and terms are therefore only constants, names or variables.
  % Our proofs do not involve timed rewrite rules or processes, i.e., are also valid in an untimed setting.
  % We recall that the results stated in the body of the paper regarding this fragment are summarised by the proposition:

  \propLowerPure*

  We prove this property by reduction from \qbf.
  Let thus \(\Psi\) be an instance of \qbf, and consider the following bounded process, given two distinct constants \(\0,\1 \in \sig_0\):
  \[P = \EventP\ A(\0); \EventP\ A(\1); \EventP\ \finishfun; 0\]
  We then define below, in polynomial time, a guarded formula \(\langle\Psi\rangle\):
  \begin{align*}
    \langle\exists x.\, \Psi\rangle & = \exists x.\, \ltlfinally A(x)_\pi \wedge \langle\Psi\rangle \\
    \langle\Psi \wedge \Psi'\rangle & = \langle\Psi\rangle \wedge \langle\Psi'\rangle \\
    \langle \neg \Psi\rangle & = \neg \langle\Psi\rangle
  \end{align*}
  The reduction is then formalised by the following lemma:

  \begin{lemma}[Correctness of the reduction]
    The \qbf input \(\Psi\) is valid \textit{iff} the following holds:
    \[P \models \forall \pi. (\ltlfinally \finishfun_\pi) \Rightarrow \langle\Psi\rangle\]
  \end{lemma}

  \begin{proof}
    For simplicity, we interpret a variable assignment of \(\Psi\) as a substitution from these variables to \(\{\0,\1\}\), in the natural way.
    Let then \(T : (P \Cstep{A(\0)A(\1)\finishfun} Q) \cdot t_\infty\) be a trace of \(P\) such that \(\ltlfinally \finishfun\).
    In particular, for all terms \(x\), \(P,\{\epsilon \mapsto \epsilon_\infty, \pi \mapsto T\} \models \ltlfinally A(x)\) \textit{iff} \(x \deduce[\emptyset] \0\) or \(x \deduce[\emptyset] \1\).
    % 
    The proof of the lemma then follows from the following more general property, that can easily be proved by a straightforward induction on \(\Psi\):
    if \(\Psi\) is a quantified boolean formula, potentially with a set of free variables \(X\), then for all substitutions \(\sigma\) such that \(\dom(\sigma) = X\) and \(\im(\sigma) \subseteq \{\0,\1\}\), \(\Psi\sigma\) is valid \textit{iff}
    \[P,\{\epsilon \mapsto \epsilon_\infty, \pi \mapsto T\} \models \langle\Psi\sigma\rangle\,.\qedhere\]
  \end{proof}




  % \subsection{Lower Bound for Non-Relational Logics} \label{app:lower-pure-nonrel}

  %   We first prove the lower bounds of Proposition~\ref{prop:lower-pure} in the cases of \piltl and \pictl.
  %   We first prove the \pspace hardness of \pictl by reduction from \qbf, and the co\np hardness of \piltl will be an easy consequence of the construction.
  %   Let thus \(\Psi\) be a \qbf input, and let us construct a bounded process \([\Psi]_P\) (built from an empty signature and rewriting system) and a guarded \pictl formula \([\Psi]_\varphi\) such that \([\Psi]_P \models [\Psi]_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
  %   Without loss of generality, we can assume up to a polynomial preprocessing that \(\Psi\) is of the form \(\Psi = Q_1 x_1, \ldots, Q_n x_n, \Phi\), where \(Q_i \in \{\forall, \exists\}\).
  %   First of all, we give an encoding of boolean formulae as bounded processes of the pure \(\pi\) calculus.
  %   We let \(\0,\1 \in \sig_0\) modelling booleans, and we see a formula \(\Phi\) as a boolean circuit;
  %   intuitively, we model the circuit flow by private communications of \(\0,\1\) constants, performed on fresh private channels modelling circuit edges.
  %   A gate \(G\) of \(\Phi\) with two input edges \(e_1,e_2\), one output edge \(e_3\), and computing a boolean function \(f : \{0,1\}^2 \to \{0,1\}\), is encoded as the process:
  %   \begin{align*}
  %     P_G & = \InP[e_1]{x_1}; \InP[e_2]{x_2}; \prod_{b,b' \in \{0,1\}} Q_G^{b,b'} \\
  %     \text{with}\ Q_G^{b,b'} & = \EventP\ \posfun(x_1,b); \EventP\ \posfun(x_2,b'); \OutP[e_3]{f(b,b')}; 0
  %   \end{align*}
  %   and where \(e_1,e_2,e_3\) are fresh names used here as private channels, and the \(\prod\) indexed operator refers to the parallel composition of processes.
  %   The \(\posfun\) event is used for the encoding of conditionals, glossed over in Section~\ref{sec:extensions}, but detailed here for the sake of rigour.
  %   If \(\Phi\) is a boolean formula (taking \(m\) arguments), we thus write 
  %   \begin{align*}
  %     x \leftarrow \Phi(u_1, \ldots, u_m); P & & \text{\(u_i\) terms}
  %   \end{align*}
  %   instead of the following process, seeing \(\Phi\) as a boolean circuit with input edges \(e_1, \ldots, e_m\), output edge \(e\), and gates \(G_1, \ldots, G_p\):
  %   \[\prod_{i=1}^m \OutP[e_i]{u_i}; 0 \mid \prod_{i=1}^p P_{G_i} \mid \InP[e]{x}; P\]
  %   We can then define the process of the reduction as:
  %   \[\begin{array}{r@{\ \ }l}
  %     [\Psi]_P =  
  %       & \InP{x_1}; \EventP\ \quantifun_1; \\
  %       & \ \ \vdots \\
  %       & \InP{x_n}; \EventP\ \quantifun_n; \\
  %       & x \leftarrow \Phi(x_1, \ldots, y_n); \EventP\ \resfun(x); 0
  %   \end{array}\]
  %   To define the formula \([\Psi]_\varphi\), we introduce an intermediary formula \(H_\pi\) intuitively modelling that the structural assumptions of the process are satisfied in the trace \(\pi\), i.e., that the event \(\posfun(u,v)\) represents an equality test between \(u\) and \(v\), and that the input terms \(x_i\) need to be booleans.
  %   We define it as \(H_\pi = H_\pi^1 \wedge H_\pi^2\), where
  %   \begin{align*}
  %     H_\pi^1 & = \ltlglobally(\forall x,y.\, \posfun(x,y) \Rightarrow \exists z.\, x \deduce[\pi] z \wedge y \deduce[\pi] z) \\
  %     H_\pi^2 & = \ltlglobally\left(\forall X.\, \InP{X}_\pi \Rightarrow X \deduce[\pi] \0 \vee X \deduce[\pi]\1\right)
  %   \end{align*}
  %   The formula of the reduction can then be defined inductively as \([\Psi]_\varphi = [\Psi]_\varphi^n\), where:
  %   \begin{align*}
  %     [\Psi]_\varphi^0 =\ & \exists \pi.\, \ltlfinally \resfun(1)_\pi \\
  %     [\Psi]_\varphi^{i+1} =\ 
  %       & \forall \pi.\, H_{\pi} \Rightarrow \ltlglobally(\quantifun_{i+1} \Rightarrow [\Psi]_\varphi^i) & \text{if \(Q_{i+1} = \forall\)} \\
  %     & \exists \pi.\, H_{\pi} \wedge \ltlfinally(\quantifun_{i+1} \wedge [\Psi]_\varphi^i) & \text{if \(Q_{i+1} = \exists\)}
  %   \end{align*}
  %   A straightforward induction on \(n\) then gives the expected correctness property, for the \pspace and the co\np reduction:
    
  %   \begin{proposition}[Correctness of the reduction]
  %     \([\Psi]_\varphi\) is a guarded \pictl formula, and \([\Psi]_P \models [\Psi]_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
  %     Besides, if \(\Psi\) is a \sat formula, \([\Psi]_P \models \forall \pi.\, H_{\pi} \Rightarrow \ltlglobally \neg 1_\pi\) \textit{iff} \(\sem{\Psi} = 0\).
  %   \end{proposition}

  % \subsection{Lower Bound for Relational Logics} \label{app:lower-pure-rel}

  %   To complete the proof of Proposition~\ref{prop:lower-pure}, it suffices to prove that \verif is also \pspace hard for \piltl[Hyper] formulae in our context.
  %   We thus let as in Section~\ref{app:lower-pure-nonrel} a \qbf formula of the form \(\Psi = Q_1 x_1, \ldots, Q_n x_n, \Phi\), with \(Q_i \in \{\forall, \exists\}\).
  %   It then suffices to construct a guarded \piltl[Hyper] formula \(\langle\Psi\rangle_\varphi\) and a bounded process of the pure \(\pi\) calculus \(\langle \Psi\rangle_P\) such that \(\langle \Psi \rangle_P \models \langle \Psi \rangle_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
  %   % 
  %   We define the process similarly as in Section~\ref{app:lower-pure-nonrel}, more precisely:
  %   \[\langle\Psi\rangle_P = \EventP\ \quantifun_0; [\Psi]_P\]
  %   The formula \(\langle\Psi\rangle_\varphi\), we will intuitively define a substantially equivalent formula to \([\Psi]_\varphi\), replacing nested path quantifications by toplevel path quantifications and relations between them.
  %   Formally, we consider the following formula, given path variables \(\pi\) and \(\pi'\):
  %   \[\pi \equiv \pi'\ \eqdef\ \forall X.\, \InP{X}_\pi \Leftrightarrow \InP{X}_{\pi'}\]
  %   It intuitively formalises that \(\pi\) and \(\pi'\) perform the same input (if any) at the current time.
  %   We also refer to the formula \(H_\pi\) defined in Section~\ref{app:lower-pure-nonrel}.
  %   We define \(\langle \Psi \rangle_\varphi = Q_1 \pi_1 \ldots Q_n \pi_n. \exists \pi.\, \langle\Psi\rangle^1_\varphi\), where:
  %   \begin{align*}
  %     \langle\Psi\rangle^{n+1}_\varphi =\ &
  %       H_\pi \wedge (\pi \equiv \pi_n \ltluntil \resfun(1)_\pi) \\
  %     \langle\Psi\rangle^i_\varphi =\ &
  %       (H_{\pi_i} \wedge (\pi_i \equiv \pi_{i-1} \ltluntil (\quantifun_{i-1})_{\pi_i})) \Rightarrow \langle\Psi\rangle^{i+1}_\varphi & \text{if \(Q_i = \forall\)} \\
  %     \langle\Psi\rangle^i_\varphi =\ &
  %       H_{\pi_i} \wedge (\pi_i \equiv \pi_{i-1} \ltluntil (\quantifun_{i-1})_{\pi_i}) \wedge \langle\Psi\rangle^{i+1}_\varphi & \text{if \(Q_i = \exists\)}
  %   \end{align*}
  %   with the convention \(\pi_{-1} = \pi_0\).
  %   A quick induction on \(n\) then gives:

  %   \begin{proposition}[Correctness of the reduction]
  %     \(\langle\Psi\rangle_\varphi\) is a guarded \piltl[Hyper] formula, and \(\langle\Psi\rangle_P \models \langle\Psi\rangle_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
  %   \end{proposition}