\section{Reminders of Complexity Theory} \label{app:prelim}

  In this section, we recall standard definitions and results of computational complexity theory that we use in several of our contributions.

  \subsection{Basic Notions}
    \paragraph{Time and Space}
      Given \(f: \setN \to \setN\), we define \(\complexitytime(f(n))\) (resp. \(\complexityspace(f(n))\)) to be the class of problems decidable by a deterministic Turing machine running in time (resp. in space) at most \(f(n)\) for input tapes of length \(n\). 
      In particular:
      \begin{itemize}
        \item \(\ptime = \bigcup_{p \in \setN} \complexitytime(n^p)\) is the class of problems decidable in polynomial time;
        \item \(\pspace = \bigcup_{p \in \setN} \complexityspace(n^p)\) is the class of problems decidable in polynomial space;
        \item \(\exptime = \bigcup_{p \in \setN} \complexitytime(2^{n^p})\), \(\expspace = \bigcup_{p \in \setN} \complexityspace(2^{n^p})\) are their exponential analogues.
      \end{itemize}
      
      When considering non-deterministic Turing machines instead of deterministic ones as above, we add a ``N'' prefix to the name of the class, leading, e.g., to \np (non deterministic polynomial time) and \nexp (non deterministic exponential time).
      Given a (non-deterministic) class \(\C\), we call co\(\C\) the class of problems whose negation is in \(\C\).

    \paragraph{Alternation}
      We also recall that \emph{alternating Turing machines} are non-deterministic Turing machines whose states are partitioned between \emph{universal states} (or \(\forall\)-states) and \emph{existential states} (or \(\exists\)-states).
      An execution from a universal (resp. existential) state is then accepting \textit{iff} any (resp. at least one) execution from this state accepts.
      In particular, non-deterministic Turing machines are alternating Turing machines with only existential states, whereas arbitrary alternation of types of states is allowed in general.
      We add a ``A'' prefix to express that we consider alternating Turing machines,
      e.g., \aptime is the class of problems decidable in alternating polynomial time.
      The following relations between classes are known:
      \begin{center}
        \ptime \(\subseteq\) \np,co\np \(\subseteq\) \pspace = \npspace = \aptime \(\subseteq\) \exptime \\
        \exptime \(\subseteq\) \nexp, co\nexp \(\subseteq\) \expspace = \nexpspace = \aexptime
      \end{center}

    % \paragraph{Reductions}
    %   We also consider in a couple of results the more general \emph{oracle} reduction, deciding a problem with access to a black constant-time black box algorithm for an other problem. 
    %   The class of problems decidable in \(\C\) with an oracle for a problem \(Q\) is noted \(\C^Q\)., or \(\C^{\D}\) when \(Q\) is complete for a class \(\D\) w.r.t. a notion of reduction executable in \(\C\).


  \subsection{Polynomial and Exponential Hierarchies} 
    The difference between \np and \pspace = \aptime is the capacity to express alternation, as \np is restricted to purely existential states.
    This is particular visible when studying complete problems for these complexity class.
    A classical \np complete problem is \sat:
    given a boolean formula \(\varphi\) of variables \(x_1, \ldots, x_n\), does 
    \[\exists x_1 \ldots \exists x_n.\, \varphi\] 
    hold? 
    On the contrary, the usual complete problem for \pspace is \qbf:
    given a boolean formula \(\varphi\) of variables \(x_1,y_1, \ldots, x_n,y_n\), does 
    \[\forall x_1. \exists y_1 \ldots \forall x_n. \exists y_n.\, \varphi\] 
    hold?
    A gradual hierarchy of classes between \np and \pspace, the \emph{polynomial hierarchy} \polyh, captures problems whose decision require only a bounded number of \(\forall\)-\(\exists\) state alternations.
    We consider here in the analogue hierarchy between \nexp and \expspace, with a distinction between constant and polynomial alternations.
    For simplicity, we omit the technical definitions of the classes and only provide here their usual characterisation:

    \begin{proposition}
      \exph (resp. \exphpoly), called the exponential hierarchy (resp. polynomially-bounded exponential hierarchy), is the class of problems decidable in exponential time by alternating Turing machines with a constant (resp. polynomial) number of alternations.
      We have \exptime \(\subseteq\) \exph \(\subseteq\) \exphpoly \(\subseteq\) \expspace.
    \end{proposition}

  \subsection{Complete Problems}

    We use \emph{many-to-one} polynomial-time reductions to characterise complete problems for given complexity classes.
    We list here those that we will use to obtain our lower bounds in Appendices~\ref{app:low-pure} and \ref{app:low-subterm}.
    They are various problems of propositional logic, that can be all presented in the following general formalism.
    A \emph{quantified boolean second-order formula} (qbsf) is defined by the following grammar:
    \[\begin{array}{r@{\ \ }l@{\qquad}l}
      \varphi, \psi\ ::=
        % & \0 \quad \1 & \text{\descrfont constants} \\
        & \varphi \wedge \psi \quad \neg \varphi & \text{\descrfont logical operators} \\
        & f^n(\varphi_1, \ldots, \varphi_n) & \text{\descrfont application} \\
        & \exists f^n.\, \varphi & \text{\descrfont quantifier}
    \end{array}\]
    where \(f^n\) is called a \emph{boolean function symbol}.
    Given a mapping \(\I\) from the free boolean function symbols of a qbsf \(\varphi\) to actual boolean functions \(F : \{0,1\}^n \to \{0,1\}\) of same arity, the \emph{interpretation} of \(\varphi\) is the boolean \(\sem{\varphi}_\I \in \{0,1\}\) inductively defined as:
    \begin{align*}
      % \sem{c}_\I & = c \qquad \text{if \(c \in \{0,1\}\)} \\
      \sem{\varphi \wedge \psi}_\I & = \sem{\varphi}_\I \cdot \sem{\psi}_\I \\
      \sem{\neg \varphi}_\I & = 1 - \sem{\varphi}_\I \\
      \sem{f^n(\varphi_1, \ldots, \varphi_n)}_\I & = F(\sem{\varphi_1}_\I, \ldots, \sem{\varphi_n}_\I) \qquad \text{with \(F = \I(f^n)\)}\\
      \sem{\exists f^n.\, \varphi}_\I & = \max \left\{\sem{\varphi}_{\I \cup \{f^n \mapsto F\}} \mid F : \{0,1\}^n \to n\right\}
    \end{align*}
    In general, we only consider qbsf whose symbols are all quantified, and therefore write \(\sem{\varphi}\) instead of \(\sem{\varphi}_\emptyset\).
    Note that usual constructions of propositional logic can be encoded in this formalism.
    \begin{itemize}
      \item \emph{Boolean variables} are simply boolean function symbols of arity 0. We will write variables \(x, y, z,\ldots\) for simplicity.
      \item \emph{Constants} 0 and 1 can be encoded as \(x \wedge \neg x\) and \(x \vee \neg x\) for some fresh variable \(x\).
      \item \emph{Logical operators} such as \(\varphi \vee \psi = \neg(\neg \varphi \wedge \neg \psi)\), \(\varphi \Rightarrow \psi = \neg \varphi \vee \psi\), and \(\forall f^n.\, \varphi = \neg (\exists f^n.\, \neg \varphi)\).
      We assume in the following that qbsf have been put in \emph{negation normal form}, i.e., that negations \(\neg\) have been pushed inwards using the above identities.
    \end{itemize}
    The classical decision problem \sat can be stated as follows:
    \problemdescr
      {A qbsf \(\varphi\) whose quantifiers are all of the form \(\exists x.\, \psi\), with \(x\) a variable, when put in negation normal form.}
      {\(\sem{\varphi} = 1\)?}
    The variation considering not only existentially quantified variables but arbitrary (alternation of) variable quantifiers is called \qbf:
    \problemdescr
      {A qbsf \(\varphi\) whose quantifiers are all of the form \(\exists x.\, \psi\) or \(\forall x.\, \psi\), with \(x\) a variable, when put in negation normal form.}
      % of the form \(\varphi = \forall x_1. \exists y_1 \ldots \forall x_n. \exists y_n.\, \psi\) with \(\psi\) a qbsf without quantifiers.}
      {\(\sem{\varphi} = 1\)?}
    Finally, the most general instance is the \qbsf problem:
    \problemdescr
      {A qbsf \(\varphi\).}
      {\(\sem{\varphi} = 1\)?}
    We summarise the complexity of these problems below.
    The cases of \sat and \qbf are textbook material, while \qbsf is taken from~\cite{L16}.

    \begin{proposition}
      \sat, \qbf and \qbsf are respectively \np complete, \pspace complete, and \exphpoly complete.
    \end{proposition}
    