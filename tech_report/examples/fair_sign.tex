\subsection{Fair Contract Signing} \label{sec:fair-signing}
  To conclude, we present a contract signing protocol~\cite{BN00}, where two parties \(A\) and \(B\) want to sign a common contract \(\Gamma\).
  Neither of the parties wants to sign the contract if it is not ensured that the other party will also sign. 
  For that we use a \emph{verifiable-timed signature} (VTS)~\cite{TBM20}: 
  this notion is similar to timed commitments, except that whatever is inside the commitment is guaranteed to be a valid signature on a chosen string.
  % 
  The protocol fixes a security time parameter \(d = 2^\eta\) (typically \(\eta = 128\)), and proceeds as follows:
  \begin{enumerate}
    \item \(A\) computes a VTS with time parameter \(d\) on \(\Gamma\) and sends it to \(B\), and so does \(B\), sending it to \(A\);
    \item if \(d = 1\), or if either party failed to send a valid VTS, force the one obtained at the previous round. 
    Otherwise, repeat the steps with time parameter \(d' = \frac{d}{2}\).
  \end{enumerate}
  % 
  One round of the protocol is unfair:
  whoever signs first can be worse off, as the other may simply force the VTS and not send their own signature.
  This is compensated by making a force opening prohibitively hard in the first round (\(d = 2^\eta\)), and then incrementally easing it until reaching \(d = 1\).
  If one party interrupts the protocol at step \(i\), they may obtain a VTS with parameter \(d = 2^{\eta-i}\), but the other party obtained a VTS with parameter \(2d = 2^{\eta-i+1}\) during the previous round.
  In short: one may worse off a party only by a factor of 2 in terms of computation effort.
  % 
  To model this, we use:
  \begin{align*}
    \sig =\  
    & \sign[x,y,z], \pk[x], \VTS[x,d], \verify[x,y,z], \solve[x] \\
    \R =\ 
    & \solve(\VTS(\sign(x,y,z),d)) \rewrite[d] \sign(x,y,z) & & \\
    & \verify(\VTS(\sign(x,y,z),d), x, \pk(z)) \rewrite \okfun & &
  \end{align*}
  The second rewrite rule checks that a given a VTS contains a signature on \(x\) by the owner of the public key \(\pk(z)\).
  We use three arguments for the \(\sign\) function, the second one being a placeholder for randomness.
  % 
  We then model fairness through a security game:
  \begin{enumerate}
    \item the adversary \(B\) engages in protocol rounds with an honest agent \(A\) (and completes at least one);
    \item when \(B\) computes a signature of \(A\) on \(\Gamma\), say, at time \(T\), they can challenge \(A\): the latter wins the game \textit{iff} it manages to compute \(B\)'s signature on \(\Gamma\) in time \(2T\) starting from the challenge time. 
  \end{enumerate}

  To model this practically, we let \(\sks_A \in \N\) be a name modelling the signing key of \(A\), and \(\Gamma,\sks_B \in \sig_0\) be two constant modelling the contract and the signing key of the adversary \(B\).
  We use internal communications on private channels \(e_1,e_2 \in \N\) to model state passing from one round to the other, which enforces in addition that rounds are executed sequentially.
  % The state in question stores the current time parameter (using \(e_1\)) and the last VTS received by \(A\) (in \(e_2\)).
  % Intuitively, updating a value ``stored'' in a private channel \(e\) is done by inputting on this channel and outputting back the new value.
  The protocol can then be modelled as follows, writing \(S_{I,d} = \VTS(\sign(\Gamma,r_I,\sks_I), d)\) a term modelling a signature of the agent \(I\) on \(\Gamma\):
  \[\begin{array}{r@{\ }l}
    A =\ 
    & \OutP{\pk(\sks_A)}; (\initproc \mid \challengeproc \mid \BangP \roundproc) \\[1mm]
    % 
    \initproc =\  
    & \InP{d};\ \OutP[e_1]{\textstyle\frac{d}{2}}; \\
    & \NewP r_A;\ \NewP r_B;\ \OutP{S_{A,d}};\ \OutP[e_2]{S_{B,d}}; 0 \\[1mm]
    % 
    \roundproc =\ 
    & \InP[e_1]{d};\ \NewP r_A;\ \OutP{S_{A,d}}; \\ 
    & \InP{\vtsmsg};\ \IfP\ \verify(\vtsmsg,\Gamma,\pk(\sk_B)) = \okfun\ \ThenP \\ 
    & \quad \OutP[e_1]{\textstyle\frac{d}{2}};\ \InP[e_2]{\statefun};\ \OutP[e_2]{\sigmsg}; 0 \\[1mm]
    % & \ElseP\ 0 \\
    % 
    \challengeproc =\ 
    & \InP[e_2]{\statefun}; \InP{\sigmsg}; \\
    & \EventP\, \challengefun(\mathit{check}) \stamp{T};\\%[-0.5mm]
    & \qquad \textit{\small with \(\mathit{check} = \verify(\VTS(\sigmsg,0), \Gamma, \pk(\sks_A))\)} \\
    & \EventP\ \losefun(\solve(\statefun)) \stamp{t}\ \WhenP\ t < 3T; 0
  \end{array}\]
  
  The initial output of \(\pk(\sks_A)\) reveals the verification key to the adversary, and \(\initproc\) encodes a first, mandatory round of the protocol.
  The process \(\BangP \roundproc\) then models optional rounds: 
  the agent retrieves the current value of the parameter on \(e_1\), sends their VTS and verifies the adversary's, and then updates the internal state on \(e_2\).
  At any point, the adversary may start to execute \(\challengeproc\): 
  they provide at time \(T\) the signature \(\sigmsg\) of \(A\), who is then tasked to retrieve the adversary's signature within a \(2T\) window, i.e., by time \(t = 3T\).
  % 
  Fairness is thus stated as a liveness property:
  ``\emph{whenever the adversary \(B\) issues a challenge, \(A\) can meet the challenge in time}''.
  % We thus first consider the following formula:
  \[\varphi = \ltlglobally(\challengefun_\pi(\okfun) \Rightarrow \ltlfinally \exists x.\, \losefun_{\pi'}(x))\]
  To avoid the situation of \(A\) losing by simply waiting \(2T\) units of time idly, the actual security formula includes an assumption of what we call here \emph{active-time progress}:
  \[\forall \pi.\, (\progress \wedge \activeness_\pi) \Rightarrow \varphi\]
  Here \(\activeness_\pi = \ltlglobally(\neg \stuck \Rightarrow \ltlfinally \neg \silent_\pi)\) (\emph{time activeness}), namely, a non-stuck process always perform at least one action in the future.
  