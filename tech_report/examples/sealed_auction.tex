\subsection{Sealed-Bid Auctions}
  We sketch a protocol for sealed-bid auctions~\cite{BN00}, where multiple parties engage in a Vickrey auction without the aid of a trusted auctioneer.
  This protocol uses the same theory for timed commitment that we used for our motivating example.
  We assume that the bidding phase has a (conservatively set) maximum duration of \(d\).
  \begin{enumerate}
    \item All participants send a \(d\)-timed commitment on their bid.
    \item \label{it:auction-open} After time \(d\), honest participants open their commitment.
    \item Then, they force the commitments of parties that did not provide an opening, and compute the winner.
  \end{enumerate}

  % Note that, similarly to the opening phase in our coin-flip running example, the step~\ref{it:auction-open} of the protocol is not strictly necessary as one may directly jump to the next step and force open all puzzles.
  % This step is therefore rather here for practical efficiency.

  We give here a general model of the protocol for an unbounded number of participants.
  If a process \(P\) models the behaviour of an honest participant of an auction parametrised by an ending time \(t_f\), the overall process can be modelled as follows, given \(d \in \TX\):
  \[\begin{array}{r@{\ }l}
    A[P] =\ 
    & \InP{d};\ \EventP\ \startfun \stamp{t_0};\\ 
    & (\EventP\ \stopfun \stamp{t_1}\ \WhenP\ t_1 < t_0+d; 0) \mid \BangP P(t_0+d)
  \end{array}\]
  The auction lasts at most a duration \(d\) (event \(\stopfun\)), involves an arbitrary number of honest participants (process \(\BangP P(t_0+d)\)) and an implicit coalition of an arbitrary number of dishonest participants (impersonated by the adversary).
  % The event \(\stopfun\) indicates the end of the auction (and will be the deadline where the bids' strong secrecy may expire).
  The process defining one participant, with bid \(x\) for an auction ending at time \(t_f\), is:
  \[\begin{array}{r@{\ }l@{}}
    Q(x,t_f) =\ 
    & \NewP r;\ \OutP{\commit(x,r,d)}; \\
    & (\OutP{x}\stamp{t}\ \WhenP\ t > t_f; \OutP{r}; 0)\ \mid\ \BangP \mathit{R(t_f)} \\[1mm]
    % 
    \mathit{R(t_f)} =\ 
    & \InP{y}\stamp{t}\ \WhenP\ t < t_f;\ \EventP\ \waitfun(y);\\
    & (\InP{z}; (\mathit{Open} + F)) + F \\[1mm]
    % 
    \mathit{Open} =\
    & \InP{s}; \IfP\ \open(y,z,s) = \okfun\ \ThenP\ \EventP\ \getfun(z); 0\ \ElseP\ F \\
    F =\ 
    & \EventP\ \getfun(\force(y)); 0
  \end{array}\]
  The process \(R\) receives a single bid and opens it (event \(\getfun\));
  the process is replicated to model an arbitrary number of receptions.
  % 
  The desired security properties are then that:
  \begin{enumerate}
    \item \label{it:auction-liveness}
    no one can be prevented from computing the winner;
    \item \label{it:auction-secret}
    the bids remain secret until the end of the auction.
  \end{enumerate}
  To express the liveness property~\eqref{it:auction-liveness}, we refer to process \(A[P]\), with 
  \begin{align*}
    P(t_f) = \NewP \mathit{bid}; Q(\mathit{bid}, t_f)
  \end{align*}
  that should verify the formula \(\forall \pi.\, \progress \Rightarrow \varphi\), with:
  \[\varphi = \ltlglobally(\forall x,y,d.\, \waitfun(\commit(x,y,d))_\pi \Rightarrow \ltlfinally \getfun(x)_\pi)\]

  Regarding Property~\eqref{it:auction-secret}, similarly as before, we model time limited strong secrecy through a security game where the adversary may spawn a number of (honest) auction participants of their choice, crafts, for each, a pair of bids \(x_0,x_1\), and then attempts to distinguish two hypothetical auctions where the first and second bids are used, respectively.
  We therefore consider the following processes \(P_i\):
  \begin{align*}
    P_i(t_f) & = \InP{x_0};\ \InP{x_1};\ Q(x_i,t_f) & i \in \{0,1\}
  \end{align*}
  and model the property by the following formula:
  \[\forall \pi_0 \colon\! A[P_0].\, \exists \pi_1 \colon\! A[P_1].\, \ltlfinally \stopfun_{\pi_0} \Rightarrow \pi_0 \StatEq \pi_1 \ltluntil \stopfun_{\pi_0}\]
