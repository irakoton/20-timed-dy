\section{Lower Bounds With a Subterm Convergent Rewriting System} \label{app:low-subterm}

  We prove in this section the complexity lower bounds for bounded processes of the applied \(\pi\) calculus when the rewriting system is subterm convergent.
  % , and the formula is guarded as in our other results.
  Our reduction only relies on a rudimentary, fixed rewriting system that makes it likely to be applicable to most classes of rewriting systems of interest.
  % We recall that the results stated in the body of the paper regarding this fragment are summarised by the proposition:

  \propLowerSubterm*

  % The co\np hardness follows from the stronger result in the pure \(\pi\) calculus, proved in Appendix~\ref{app:lower-pure-nonrel}.
  % We therefore only prove the \exphpoly hardness of \verif for \pictl and \piltl[Hyper].

  \subsection{Lower Bound for Non-Relational Logics} \label{app:lower-exph-pictl}

    We prove here the lower bound for \pictl guarded formulae, by reduction from \qbsf.
    Let thus \(\Psi\) be a qbsf and let us construct (in polynomial time) a subterm convergent rewriting system \(\R\), a bounded process \([\Psi]_P\) and a guarded formula \([\Psi]_\varphi\) such that \(\sem{\Psi} = 1\) \textit{iff} \([\Psi]_P \models [\Psi]_\varphi\).
    Up to a linear preprocessing on \(\Psi\), we can assume without loss of generality that it is of the form:
    \begin{align*}
      Q_1 f_1^{n_1}, \ldots, Q_k f_k^{n_k}, \psi 
    \end{align*}
    where \(Q_i \in \{\forall, \exists\}\), \(n_i \in \N\), and \(\psi\) does not contain quantifiers.
    Intuitively, the process \([\Psi]_P\) will receive a term \(u_i\) from the attacker for each \(f_i^{n_i}\), verify that it is a valid encoding of a truth table for a boolean function of arity \(n_i\), and finally compute the corresponding outcome of the boolean formula \(\psi\).
    Formally, we will encode a truth table for a \(n_i\)-ary function \(f\) as a binary tree of depth \(n_i\), whose leaf at position \(b_0 \ldots b_{n_i}\) is the boolean \(f_i(b_0, \ldots, b_{n_i})\).
    To model binary trees and boolean computations, we consider the following signature \(\sig\) and rewriting system \(\R\), assuming \(\0,\1,\okfun \in \sig_0\):
    \[\begin{array}{r@{\,}l}
      \sig : & \nodefun/2,\ \childfun/2,\ \testnode/1,\ \testbool/1, \conjunction/2, \negation/1 \\
      \R : & \childfun(\nodefun(x,y),\0) \to x \\
      & \childfun(\nodefun(x,y),\1) \to y \\
      & \testnode(\nodefun(x,y)) \to \okfun \\
      & \testbool(b) \to \okfun \quad \text{for \(b \in \{\0,\1\}\)}\\
      & \conjunction(b_1,b_2) \to b_1 \cdot b_2 \quad \text{for \(b_1,b_2 \in \{\0,\1\}\) interpreted as \(0,1\)}\\
      & \negation(b) \to 1-b \quad \text{for \(b \in \{\0,\1\}\) interpreted as \(0,1\)}
    \end{array}\]
    The test functions are used to verify that a term has a given tree structure.
    Note also that the rewrite rules for \(\conjunction\) and \(\negation\) are effectively subterm convergent, as \(b_1 \cdot b_2\) and \(1-b\) refer to the term obtained by interpreting \(b_1,b_2,b \in \{\0,\1\}\) as the corresponding integer (\(0\) or \(1\)), resulting in a total of 6 rewrite rules with a constant right-hand side.
    We define in addition a syntactic sugar for the iterative application of the extraction symbol \(\childfun\).
    If \(u\) is a term and \(w \in \{\0,\1\}^*\) is a finite sequence of \(\0\)'s and \(\1\)'s, we define \(\childfun^*(u,w)\) to be the term inductively defined as
    \[\begin{array}{r@{\,}ll}
      \childfun^*(u,\epsilon) & = u \\
      \childfun^*(u,b \cdot w) & = \childfun^*(\childfun(u,b),w) & \text{if \(b \in \{\0,\1\}\)}
    \end{array}\]
    In particular, we interpret the boolean formula \(\psi\) as a term \([\psi]\) of the applied \(\pi\) calculus, using the following inductive translation;
    it assumes a collection of terms \(\vec{u}\) associating a term \(u_i\) to each function symbol \(f_i\) (intuitively encoding its truth table):
    \[\begin{array}{r@{\,}l}
      {[f_i(\psi_1, \ldots, \psi_{n_i})]} & = \childfun^*(u_i,[\psi_1] \cdots [\psi_{n_i}]) \\
      {[\psi_1 \wedge \psi_2]} & = \conjunction([\psi_1],[\psi_2]) \\
      {[\neg \psi]} & = \negation([\psi])
    \end{array}\]
    The overall structure of the process \([\Psi]_P\) is then outlined in Figure~\ref{fig:structure-lower-pictl} and detailed below.
    Intuitively, the generation of each truth table \(u_i\) of \(f_i\) is indicated by an event \(\getfun_i(u_i)\), the process \(\verifproc[i](u_i)\) verifies that it is well-formed, and the final event \(\resfun([\psi])\) outputs the result of \(\psi\) according to the above translation \([\psi]\).

    % \input{fig_reduction_exph}

    Formally, we define \([\Psi]_P = [\Psi]_P^1\), where \([\Psi]_P^i\) is defined inductively as follows:
    \begin{align*}
      \text{if \(i \leqslant k\),}\ [\Psi]_P^i =\ 
        & \InP{u_i}; \EventP\ \getfun_i(u_i); \left([\Psi]_P^{i+1} + \verifproc[i](u_i)\right) \\[1mm]
      % 
      \text{with}\ \verifproc[i](u_i) =\ 
        & \EventP\ \startfun_i; (O \mid I) \\
      \text{with}\ O =\ 
        & \textstyle \prod_{i=1}^{n_i} (\OutP[e_i]{\0}; 0) + (\OutP[e_i]{\1}; 0) \\
      \text{and}\ I =\ 
        & \InP[e_1]{x_1}; \ldots; \InP[e_{n_i}]{x_{n_i}}; \\
        & \EventP\ \leaffun(\childfun^*(u_i,x_1 \cdots x_{n_i})); 0 \\
        & \textit{\small with \(e_i\) fresh names and, as before, \(\textstyle\prod\) is an}\\[-1mm]
        & \textit{\small indexed operator for parallel composition} \\[1mm]
      % 
      \text{and finally}\ [\Psi]_P^{k+1} =\ 
        & \EventP\ \resfun([\psi]); 0
    \end{align*}
    The formula \([\Psi]_\varphi\) is then defined as \([\Psi]_\varphi = [\Psi]_\varphi^1\), with:
    \begin{align*}
      \text{if \(i \leqslant k\),}\ [\Psi]_\varphi^i =\ 
        & Q_i \pi, (\ltlglobally \neg \startfun_{i\pi})\ \textbf{OP} \\
        & \quad \ltlglobally(\forall x.\, \getfun_i(x)_\pi \Rightarrow (\varphi_{\verifproc}^i(x) \Rightarrow [\Psi]_\varphi^{i+1})) \\
        & \textit{\small with \textbf{OP} being \(\Rightarrow\) if \(Q_i = \forall\), and \(\wedge\) if \(Q_i = \exists\)} \\[1mm]
      % 
      \text{with}\ \varphi_{\verifproc}^i =\ 
        & \forall \pi.\, (\ltlfinally \startfun_{i\pi}) \Rightarrow \\
        & \quad \forall x.\, \ltlfinally \leaffun(x)_\pi \Rightarrow x \deduce[\pi] \0 \vee x \deduce[\pi] \1 \\[1mm]
      % 
      \text{and}\ [\Psi]_\varphi^{k+1} =\ 
        & \exists \pi.\, \ltlfinally \resfun(\1)_\pi
    \end{align*}

    \input{fig_reduction_exph}

    We then aim at establishing the following result:

    \begin{proposition}[Correctness of the reduction]
      If \(\Psi\) is qbsf, \([\Psi]_\varphi\) is a guarded \pictl formula, and \([\Psi]_P \models [\Psi]_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
    \end{proposition}

    This can be obtained by a straightforward induction on \(k\) provided the following two auxiliary lemmas (\ref{prop:lower-exph-verif} and \ref{lower-exph-interpret}) have been priorly established.
    The first one formalises the desired correctness property of the \(\verifproc[i]\) processes.
    
    \begin{lemma}[Correctness of structure verification] \label{prop:lower-exph-verif}
      Let \(i \in \eint{1}{k}\) and consider, for some term \(u\), the process \(P(u) = [\Psi]_P^{i+1} + \verifproc[i](u)\).
      Then we have \(P(u) \models \varphi_{\verifproc}^i\) \textit{iff} \(u\) is a complete binary tree of depth \(n_i\) with boolean leaves, that is, a term such that for all \(b_0, \ldots, b_{n_i-1} \in \{\0,\1\}\), 
      \[\childfun^*(u,b_0\cdots b_{n_i-1}) \rewrite_\R^* b \in \{\0,\1\}\]
    \end{lemma}

    \begin{proof}
      We recall that the process \(\verifproc[i](u)\) is built only from private communications on fresh private channels \(e_1, \ldots, e_{n_i}\).
      For all frames \(\Phi\) and \(\gtime{t} \in \setR[+]\), all maximal traces of \(A = (\multi{\verifproc[i](u)}, \Phi, \gtime{t}_0)\) are therefore of the form:
      \[A \Cstep{\startfun_i \cdot \leaffun(b)} (\P,\Phi,\gtime{t}_1)\]
      where \(\childfun^*(u,b_0\cdots b_{n_i-1}) \tded[\Phi]{t} b\) for some \(b_0,\ldots,b_{n-1} \in \{\0,\1\}\) and \(t \in ]\gtime{t}_0,\gtime{t}_1]\).
      Conversely, for all \(b_0,\ldots,b_{n-1} \in \{\0,\1\}\) and \(\gtime{t}_1 \geqslant t > \gtime{t}_0\), if \(\childfun^*(u,b_0\cdots b_{n_i-1}) \rewrite_\R^* b\), \(b\) in normal form, then there exists a trace of the above form.
      Let us then prove the two directions of the proposition separately.

      \smallskip\noindent{\bfseries\small(\(\Rightarrow\))}
      Let \(u\) be a term such \(P(u) \models \varphi_{\verifproc}^i\), let also \(b_0, \ldots, b_{n_i-1} \in \{\0,\1\}\) and let us show that \(b\), the normal form of \(\childfun^*(u,b_0 \cdots b_{n_i-1})\), is either \(\0\) or \(\1\).
      For that, it suffices to consider, by the preliminary discussion, a trace \(\pi\) of \(P(u)\) of the form 
      \(\pi : P(u) \Cstep{\startfun_i \cdots \leaffun(b)} Q\)
      and to use the assumption that \(P(u) \models \varphi_{\verifproc}^i\).
      
      \smallskip\noindent{\bfseries\small(\(\Leftarrow\))}
      Conversely, let us assume that \(u\) is a complete binary tree of depth \(n_i\) with boolean leaves, and let us prove that \(P(u) \models \varphi_{\verifproc}^i\).
      First of all, since the \(\startfun_i\) event does not appear in the process \([\Psi]_P^{i+1}\), we observe that it suffices to prove that 
      \[\verifproc[i](u) \models \forall \pi. \forall x.\, \ltlfinally \leaffun(x)_\pi \Rightarrow x \deduce[\pi] \0 \vee x \deduce[\pi] \1\]
      Let thus \(\pi\) be a trace of \(\verifproc[i](u)\) verifying the \(\piltl\) formula \(\ltlfinally \leaffun(b)\) for some term \(b\) in normal form.
      Since the event \(\leaffun\) can be executed at most once in \(\verifproc[i](u)\), and since the process \(\verifproc[i](u)\) contains no replication, we can assume without loss of generality that the trace \(\pi\) is maximal.
      From the preliminary discussion, we therefore get \(b_0, \ldots, b_{n_i-1} \in \{\0,\1\}\) such that \(b\) is the normal form of \(\childfun^*(u,b_0 \cdots b_{n_i-1})\), hence \(b \in \{\0,\1\}\) by hypothesis.
    \end{proof}

    The second intermediary lemma formalises the correctness of the interpretation of quantifier-free qbsf \(\psi\) as terms \([\psi]\).
    For that, we introduce the following notation.
    Given a boolean function \(f : \{0,1\}^n \to \{0,1\}\), we write \([f]\) its encoding as a binary tree of depth \(n\);
    that is, \([f]\) is the term such that for all \(b_0,\ldots,b_{n-1} \in \{\0,\1\}\), 
    \begin{enumerate}
      \item \(\childfun^*([f],b_0 \cdots b_{n-1}) \rewrite_\R^* \0\) \textit{iff} \(f(b_0, \ldots, b_{n-1}) = 0\); and
      \item \(\childfun^*([f],b_0 \cdots b_{n-1}) \rewrite_\R^* \1\) \textit{iff} \(f(b_0, \ldots, b_{n-1}) = 1\).
    \end{enumerate}
    Note that this encoding is a bijection from boolean functions of arity \(n\) to complete binary trees of height \(n\) with \(\0,\1\) leaves.
    Building on this, we obtain by a quick induction on the structure of \(\psi\):

    \begin{lemma}[Correctness of formula interpretation] \label{lower-exph-interpret}
      Let \(\psi\) be a qbsf without quantifier (but potentially with free function symbols), and an interpretation \(\I\) mapping each of its function symbols \(f^n\) to a boolean function \(\I(f) : \{0,1\}^n \to \{0,1\}\).
      We then consider the translation \([\psi]\) of \(\psi\) as a term, parametrised by the collection of terms \(\vec{u} = [\I(f_1)], \ldots, [\I(f_k)]\).
      Then we have: 
      \begin{enumerate}
        \item \(\sem{\psi}_\I = 1\) \textit{iff} \([\psi] = \1\); and
        \item \(\sem{\psi}_\I = 0\) \textit{iff} \([\psi] = \0\).
      \end{enumerate}
    \end{lemma}
    
  \subsection{Lower Bound for Relational Logics}
    
    We now present a similar reduction for the \piltl[Hyper] fragment.
    As before, we thus consider a qbsf of the form 
    \begin{align*}
      \Psi = Q_1 f_1^{n_1}, \ldots, Q_k f_k^{n_k}, \psi & & \text{\(\psi\) quantifier free}
    \end{align*}
    and we construct a bounded process \(\langle \Psi\rangle_P\) and a \piltl[Hyper] formula \(\langle\Psi\rangle_\varphi\) such that \(\langle\Psi\rangle_P \models \langle\Psi\rangle_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
    We use in particular the same rewriting system \(\R\) as for the \pictl reduction in Appendix~\ref{app:lower-exph-pictl}, and refer to previous constructions and notations:
    \begin{itemize}
      \item the processes \(\verifproc[i](u)\) defined in Appendix~\ref{app:lower-exph-pictl};
      \item the interpretation \([\psi]\) of qbsf (without quantifiers but free function symbols) as terms, assuming a term \(u_i\) implicitly associated to each function symbol appearing in \(\psi\).
    \end{itemize}
    Building on these notations, we define \(\langle\Psi\rangle_P = \sum_{i=0}^k P_i\) where:
    \[\begin{array}{@{}r@{\ }ll@{}}
      P_0 =\ 
        & \EventP\ \startfun_0; \\
        & \InP{u_1}; \EventP\ \getfun_1(u_1); \\
        & \quad \vdots \\
        & \InP{u_k}; \EventP\ \getfun_k(u_k); \\
        & \EventP\ \resfun([\psi]); 0 \\[1mm]
      % 
      P_i =\ 
        & \EventP\ \startfun_i; \InP{u_i}; \EventP\ \getfun_i(u_i); \verifproc[i](u_i) & \text{if \(i \in \eint{1}{k}\)}
    \end{array}\]
    We then define the formula \(\langle\Psi\rangle_\varphi\).
    First of all, we write \(\pi \equiv \pi'\) to express that the two traces \(\pi,\pi'\) perform the same \(\getfun\) actions at a given time, i.e., it is a shortcut for the formula:
    \[\pi \equiv \pi'\ \eqdef\ \textstyle\bigwedge_{i=1}^k \forall x.\, \getfun_i(x)_\pi \Leftrightarrow \getfun_i(x)_{\pi'}\]
    Then similarly as the notation introduced in Section~\ref{sec:extensions}, we use extended path quantifiers \(\forall \pi \colon\! P_i.\, \varphi\) and \(\exists \pi \colon\! P_i.\, \varphi\) instead of 
    \begin{align*}
      \forall \pi.\, (\ltlfinally \startfun_{i\pi}) \Rightarrow \varphi
      & & \text{and} & &
      \exists \pi.\, (\ltlfinally \startfun_{i\pi}) \wedge \varphi
    \end{align*}
    Although this encoding does not lead a \piltl[Hyper] formula in general, it can easily be converted back into one using basic identities of first-order logic.
    Writing \(\overline{\forall} = \exists\) and \(\overline{\exists} = \forall\), we define:
    \[\langle\Psi\rangle_\varphi = Q_1 \pi_1 \colon\! P_0, \overline{Q_1}\pi_1'\colon\! P_1 \ldots Q_k \pi_k \colon\! P_0. \overline{Q_k}\pi_k'\colon\! P_k. \exists \pi_{k+1} \colon\! P_0.\, \varphi_1\]
    where the path-quantifier free formulae \(\varphi_i\), \(i \in \eint{1}{2k+1}\) are inductively defined as follows, with the convention \(\pi_0 = \pi_1\):
    \[\begin{array}{@{}r@{\ }l@{}}
      \varphi_i =\ 
        & \forall x_i.\, \ltlfinally \getfun_i(x_i)_{\pi_i} \Rightarrow  \\
        & \quad (\pi_i \equiv \pi_{i-1} \ltluntil \getfun_i(x_i)_{\pi_i}) \Rightarrow \varphi_{i+1} 
        \quad \text{\small if \(i \in \eint{1}{k}\), \(Q_i = \forall\)} \\
      % 
      \varphi_i =\ 
        & \exists x_i.\, \ltlfinally \getfun_i(x_i)_{\pi_i} \wedge \\
        & \quad (\pi_i \equiv \pi_{i-1} \ltluntil \getfun_i(x_i)_{\pi_i}) \wedge \varphi_{i+1} 
        \quad \text{\small if \(i \in \eint{1}{k}\), \(Q_i = \exists\)} \\[1mm]
      % 
      \varphi_{k+i} =\ 
        & (\ltlfinally \getfun_i(x_i)_{\pi_i'} \wedge \ltlfinally (\exists b.\, \leaffun(b)_{\pi_i'} \wedge b \ndeduce[\pi_i'] \0 \wedge b \ndeduce[\pi_i'] \1)) \\
        & \quad \vee \varphi_{k+i+1}
        \quad \text{\small if \(i \in \eint{1}{k}\), \(Q_i = \forall\)}\\[1mm]
      % 
      \varphi_{k+i} =\ 
        & (\ltlfinally \getfun_i(x_i)_{\pi_i'} \Rightarrow \forall b.\, \ltlfinally \leaffun(b)_{\pi_i'} \Rightarrow (b \deduce[\pi_i'] \0 \vee b \deduce[\pi_i'] \1)) \\
        & \quad \wedge \varphi_{k+i+1}
        \quad \text{\small if \(i \in \eint{1}{k}\), \(Q_i = \exists\)}\\[1mm]
      %
      \varphi_{2k+1} =\ 
        & \pi_{k+1} \equiv \pi_k \ltluntil \resfun(\1)_{\pi_{k+1}}
    \end{array}\]

    It then remains to prove:
    
    \begin{proposition}[Correctness of the reduction] \label{prop:lower-exph-hyperltl}
      \(\langle\Psi\rangle_\varphi\) is a guarded \piltl[Hyper] formula, and \(\langle\Psi\rangle_P \models \langle\Psi\rangle_\varphi\) \textit{iff} \(\sem{\Psi} = 1\).
    \end{proposition}

    \newpage
    
    To show this, we first prove the following intermediary result.
    We say in its statement that a trace is \emph{well-formed} when all of the \(\getfun_i(u_i)\) events it contains verify that \(u_i\) is a complete binary tree of depth \(n_i\) with boolean leaves (in the same sense as in Section~\ref{app:lower-exph-pictl}).
    By extension, a mapping \(\Pi\) from path variables to traces is said to be well-formed when for all \(i \in \eint{1}{k}\),
    \begin{itemize}
      \item if \(\pi_i \in \dom(\Pi)\) then \(\Pi(\pi_i)\) is a well-formed trace executing an event \(\getfun_i(u_i)\) for some term \(u_i\);
      \item if \(\pi_i,\pi_{i+1} \in \dom(\Pi)\), then \(\Pi(\pi_i)\) and \(\Pi(\pi_{i+1})\) coincide until the event \(\getfun_i(u_i)\) included.
    \end{itemize}
    % Finally, for simplicity, when mentioning the satisfiability of a \piltl[Hyper] formula as \(T,\Pi \models \varphi\), we omit the trace \(T\) as it is only useful for path quantifiers under the scope of a \(\ltluntil\) operator.
    We finally consider the suffix of \(\langle\Psi\rangle_\varphi\) starting at index \(i \in \eint{1}{k+1}\), written using the following notations:
    \[\langle\Psi\rangle_\varphi^i = Q_i \pi_i \colon\! P_0. \overline{Q_i}\pi_i'\colon\! P_i \ldots Q_k \pi_k \colon\! P_0. \overline{Q_k}\pi_k'\colon\! P_k. \exists \pi_{k+1} \colon\! P_0.\, \varphi_1\]
    The auxiliary lemma itself is then stated and proved below, followed by the conclusion of the proof of Proposition~\ref{prop:lower-exph-hyperltl}:

    % \newpage

    \begin{lemma} \label{lem:lower-exph-hyperltl}
      Let \(i \in \eint{1}{k}\) and \(\Pi\) be a well-formed mapping with \(\dom(\Pi) = \{\pi_1, \pi_1', \ldots, \pi_{i-1},\pi_{i-1}'\}\).
      Then assuming \(Q_i = \forall\) (resp. \(Q_i = \exists\)), \(\Pi \models \langle\Psi\rangle_\varphi^i\) \textit{iff} for all well-formed traces \(T_i\) (resp. there exists a well-formed trace \(T_i\)), 
      \(\Pi \cup \{\pi_i \mapsto T_i,\pi_i' \mapsto T_i'\} \models \langle\Psi\rangle_\varphi^{i+1}\), where \(T_i'\) is an arbitrary trace of \(P_i\).
    \end{lemma}

    \begin{proof}
      Let us first handle the case \(Q_i = \forall\).
      By definition, \(\Pi \models \langle\Psi\rangle_\varphi^i\) \textit{iff} for all traces \(T_i\) (well-formed or not), 
      \[\Pi \cup \{\pi_i \mapsto T_i\} \models \exists \pi_i'\colon\! P_i.\, \langle\Psi\rangle_\varphi^{i+1}\]
      We rely now on the correctness of the \(\verifproc[i]\) process, proved in Appendix~\ref{app:lower-exph-pictl}, Proposition~\ref{prop:lower-exph-verif}.
      In particular, if \(T_i\) is well-formed, the left-hand term of the disjunction \(\varphi_{k+i}\) cannot be satisfied, and we can thus indeed let \(T_i'\) an arbitrary trace of \(P_i\) to obtain the expected result.
      On the contrary if \(T_i\) is not well-formed, then the correctness of \(\verifproc[i]\) yields a trace \(T_i'\) of \(P_i\) satisfying the left-hand term of the disjunction \(\varphi_{k+1}\), thus resulting again in \(\Pi \cup \{\pi_i \mapsto T_i,\pi_i' \mapsto T_i'\} \models \langle\Psi\rangle_\varphi^{i+1}\).
      We thus obtain the desired conclusion.
      % 
      Suppose now that \(Q_i = \exists\).
      By definition, \(\Pi \models \langle\Psi\rangle_\varphi^i\) \textit{iff} there exists a trace \(T_i\) such that 
      \[\Pi \cup \{\pi_i \mapsto T_i\} \models \forall \pi_i' \colon\! P_i.\, \langle\Psi\rangle_\varphi^{i+1}\]
      However this implies that for all traces \(T_i'\) of \(P_i\), the left-hand term of the conjunction \(\varphi_{k+i}\) is satisfied.
      In particular, again by the correctness of the \(\verifproc[i]\) process, i.e., Proposition~\ref{prop:lower-exph-verif}, we deduce that \(T_i\) is well-formed, hence the result.
    \end{proof}

    To obtain a proof for Proposition~\ref{prop:lower-exph-hyperltl}, we rely again on the interpretation of boolean formulae \(f\) as binary trees \([f]\), as defined in Appendix~\ref{app:lower-exph-pictl} and whose correctness w.r.t. the quantifier-free-qbsf interpretation \([\psi]\) is formalised in Lemma~\ref{lower-exph-interpret}.
    Using this, we can indeed associate to each well-formed mapping \(\Pi\) an interpretation \(\I\) of the function symbols of \(\Psi\) to boolean functions of adequate arity.
    Thus Proposition~\ref{prop:lower-exph-hyperltl} can be obtained by a quick (increasing) induction on \(i \in \eint{1}{k+1}\), relying on the above discussion for the case \(i = k+1\), and Lemma~\ref{lem:lower-exph-hyperltl} for inductive steps \(i \leqslant k\).