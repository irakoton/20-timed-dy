\section{Decidability and Complexity} \label{sec:decidability}

  In this section, we study the decidability of properties expressed in our framework, i.e., the following decision problem \verif:
  \problemdescr
    {A signature \(\sig\), a rewriting system \(\R\) and a process \(P\) built from them, a \pictl[Hyper] formula \(\varphi\).}
    {\(P \models \varphi\)?}

  \medskip
  This can be seen as the analogue of the model checking problem of a Kripke structure against a standard \ctl[Hyper] formula~\cite{CFK14,FRS15}.
  This latter problem is known to be decidable in general~\cite{CFK14}, and even \pspace complete in the non-relational fragments \ltl and \ctl, using techniques mostly from automata theory.
  One may wonder to which extent these results carry out to the more involved \tidy logics, in particular in regards of results for extensions of temporal logics with first-order quantifiers~\cite{HKK03}.
  However:

  \begin{proposition} \label{prop:undecidable}
    The \verif problem is undecidable for \piltl (and thus \pictl), even for processes without replication.
  \end{proposition}

  Indeed, the undecidability of \emph{deducibility} for convergent rewriting systems is a standard result in (untimed) protocol analysis~\cite{AC06};
  said differently, \verif is undecidable even for inputs of the form:
  \begin{align*}
    P & = \OutP{u_1}; \ldots; \OutP{u_n}; 0 &
    % \InP{x}; \EventP\ \mathsf{A}(x); 0 & 
    \varphi & = \exists \pi.\, \ltlfinally (\exists X.\, X \deduce[\pi] u)
    % \mathsf{A}(u)
  \end{align*}
  Note that this does not rely on time constraints, hence our comparison with untimed logics such as \ltl and \ctl.
  We propose in this section further results that draw a preliminary picture of the possibilities and limits of the problem in terms of decidability.
  Basics of complexity theory can be found in Appendix~\ref{app:prelim} but, as a minimal reminder, we give here common relations between complexity classes, including \polyh (polynomial hierarchy) and \exphpoly (polynomially-bounded exponential hierarchy):

    \begin{center}
      \ptime \(\subseteq\) \np \(\subseteq\) \polyh \(\subseteq\) \pspace \(\subseteq\) \exptime \\
      \exptime \(\subseteq\) \nexp \(\subseteq\) \exph \(\subseteq\) \exphpoly \(\subseteq\) \expspace
    \end{center}

  \subsection{Reduction to Constraint Solving} \label{sec:upper}

    Despite the overall undecidability, powerful techniques have been developed in protocol analysis to tackle more specific classes of security properties.
    A popular one for untimed bounded processes is to characterise security as a finite set of adversarial constraints~\cite{B07,CCD13,CKR18}.
    Examples include the reduction of trace equivalence (expressible in \piltl[Hyper]) to the \emph{equivalence of vectors of constraint systems}~\cite{CCD13}, a subsequent work establishing in a similar way its co\nexp completeness for a class of rewriting systems~\cite{CKR18}.
    Another similar result establishes the co\np completeness of some safety properties (expressible in \piltl), by reducing them to the solvability of simpler constraints~\cite{B07}.
    In this section, we generalise this set of techniques by exhibiting a natural notion of constraint crisply characterising the decision of arbitrary hyperproperties expressed in \pictl[Hyper].
    This provides in particular for the first time such reductions for, e.g., liveness properties.

    Our constraints are, intuitively, first order formulae expressing time conditions and computability requirements.
    They are thus similar to \pictl[Hyper] formulae, but evacuate all considerations related to traces (path variables, ``until'', actions), and keep track of relations between different attacker computations.

    \begin{definition}[Constraint]
      The grammar of \emph{constraints} is:
      \[\begin{array}{rllll@{\qquad}l}
        \Gamma\ ::=\ 
          & \forall X.\, \Gamma & \forall x.\, \Gamma & & & \text{\descrfont term quantifiers} \\
          & \Gamma \wedge \Gamma & \neg \Gamma & & & \text{\descrfont logical operators} \\
          & u \tded[\Phi]{t} v & X \tded[\Phi]{t} v & b & \xi = \zeta & \text{\descrfont atomic constraints} \\
        % \Gamma_0\ ::=\ 
        %   & u \tded[\Phi]{t} v & X \tded[\Phi]{t} v & b
      \end{array}\]
      with \(u,v\) terms, \(x \in \X\), \(X \in \X[2]\), \(t \in \TX\), \(b\) time condition, \(\xi,\zeta\) recipes that may contain second order variables, and \(\Phi\) dated frame whose domain contains elements of the form \(\ax\stamp{t}\), where \(t \in \TX\) instead of \(\setR[+]\).
      % When a constraint \(\Gamma\) has all of its variables quantified (including the temporal variables appearing in dated frames of atomic constraints),
      We say that \(\Gamma\) is \emph{valid} when it is satisfied by interpreting \(\tded[\Phi]{t}\) as dated computability, and \(=\) as syntactic equality.
    \end{definition}
      
    The classical approach to reduce security to the validity of constraints is to define a \emph{symbolic semantics} abstracting all sources of unboundedness of the baseline semantics---here, Rules~\ruletic and \ruletimein---by constraints characterising them.
    We define one in Appendix~\ref{app:reduction} for our timed calculus, and formalise its soundness and completeness w.r.t. the regular semantics for bounded processes.
    Building on it, we obtain the following result (proof in Appendix~\ref{app:reduction}):

    \begin{theorem} \label{thm:constr-reduce}
      The \verif problem is decidable for bounded processes with an oracle for testing the validity of constraints.
    \end{theorem}

    % Examples typically include the reduction, in the untimed setting, of trace equivalence (expressible in \piltl[Hyper]) to the \emph{equivalence of vectors of constraint systems}~\cite{CCD13}, a decision problem that can be expressed as the validity of some constraints in our formalism.
    % A subsequent, also constraint-based, decision procedure then established trace equivalence of bounded processes to be co\nexp complete for a subclass of subterm convergent rewrite systems~\cite{CKR18}.
    % We are interested here in another similar result, deciding in co\np some safety properties (expressible in \piltl) based on the solvability of constraints that can be expressed in our formalism under the form \(\exists \vec{\omega}, \Gamma\), where \(\Gamma\) does not contain quantifiers~\cite{B07}.
    % We however show that, by building on the more general reduction of Theorem~\ref{thm:constr-reduce}, the same constraint solving procedure can be used to significantly strengthen the result---capturing, among others, liveness properties.

    % \begin{theorem}
    %   In an untimed term algebra with subterm convergent rewriting, the \verif problem for bounded processes is: 
    %   \begin{enumerate}
    %     \item co\np complete for guarded \piltl formulae;
    %     \item \exphpoly complete for guarded \pictl formulae.
    %   \end{enumerate}
    % \end{theorem}

    % The proof of this result can be found in Appendix~\ref{app:decidability}.
    % This illustrates in particular the gain offered by the generality of the approach of Theorem~\ref{thm:constr-reduce}.
    Solving the underlying constraints is in particular undecidable in general by Proposition~\ref{prop:undecidable}.
    A promising direction for future work is therefore to study their solvability for practical, more restricted fragments to obtain decidability results similar to the aforementioned ones, for protocols built on top of timed cryptography. 
    % , by relying on the reduction of Theorem~\ref{thm:constr-reduce}.
    % In the timed setting, the above theorem would for example give the decidability of most security properties appearing in examples of this paper, provided indistinguishability-based modelling of secrecy is replaced by a use of the \(\atk\) formula, and for a fixed number of participants.



  \subsection{Negative Results} \label{sec:lower}

    As a complement, we now establish complexity lower bounds for \verif, in order to give a theoretical insight on the difficulty of various instances of the problem.
    % Complexity upper bounds would then be expectedly obtained by, e.g., constraint-solving techniques discussed in the previous section.
    We focus here on notable fragments: %considered in the (untimed) protocol analysis literature.
    \begin{enumerate}
      \item The \emph{pure \(\pi\)-calculus} corresponds to having an empty signature (but we still allow arbitrary event symbols for convenience).
      Although its interest is limited in terms of security modelling, the resulting framework is closer to standard logics such as \ctl[Hyper], making their comparative study more insightful.
      \item \emph{Subterm convergence} is a syntactic restriction on rewrite rules \(\ell \to r\), stating that \(r\) should either be a strict subterm of \(\ell\), or a term without variables in normal form.
      All rules presented in examples in this paper are subterm convergent, and several decidability results are known in the untimed case under this assumption~\cite{AC06,CCD13,CKR18}, making the class relevant to consider.
      \item \emph{Bounded processes} are processes without replication~\cite{CCD13,CKR18}.
      Replication makes even the pure \(\pi\)-calculus Turing complete, yielding many undecidability results.
      Yet, the bounded fragment is not trivially decidable (recall Proposition~\ref{prop:undecidable}), and corresponds to protocols with a fixed number of participants.
      % its practical interest is that attacks on real protocols rarely require more than 2 or 3 parallel sessions to be mounted.
      % \item \emph{Guarded formulae} is a standard syntactic criterion of formulae, required in practical tools such as \tamarin~\cite{manual-tamarin}.
      % This intuitively restricts term quantifiers to cases where only a finite number of instances need to be considered to (dis)prove the formula. 
      % , which thus yields significantly simpler constraints when applying the reduction techniques of Section~\ref{sec:upper}.
      % We formalise in this section an analogue of this property in our model.
    \end{enumerate}
    
    In addition, all of our complexity lower bounds will be stated for \emph{guarded formulae}, a standard syntactic criterion of formulae, required in practical tools such as \tamarin~\cite{manual-tamarin}.
    Its aim is to restricts term quantifiers \(\forall x, \forall X\) to configurations of the form \(\forall \vec{\omega}. \varphi_0 \Rightarrow \varphi\), where the so-called \emph{guard} \(\varphi_0\) ensures that only a finite number of instances of the variables \(\vec{\omega}\) need to be considered when proving \(\varphi\).
    We propose below a possible formalisation in our framework.

    % As formulae may contain arbitrary term quantifiers \(\forall x\) and negation \(\neg\), one may easily encode the satisfiability of \emph{Quantified Boolean Formulae}, the prototypical \pspace complete problem, into the verification of even trivial processes.
    % This results in a form of irreducible minimum complexity, even for verifying trivial processes such as the null process \(0\).
    % To get more insightful complexity lower bounds, we will hence focus on guarded formulae as they typically prevent this issue.

    \begin{definition}[Guarded quantifier]
      A \pictl[Hyper] formula 
      \[\forall \omega_1 \ldots \forall \omega_n.\, \varphi_0 \Rightarrow \varphi\]
      is called a \emph{guarded quantifier}, with \(\vec{\omega} = \omega_1, \ldots, \omega_n\) are regular or second-order variables appearing in the \emph{guard} \(\varphi_0\).
      We also require that \(\varphi_0\) is of either of the following forms:
      \begin{enumerate}
        \item \(u \deduce[\pi] x\), with \(u\) a term not containing variables of \(\vec{\omega}\), and \(x \in \X\);
        \item \(\InP[X]{Y}_\pi\) or \(\OutP[X]{Y}_\pi\), \(X,Y\) second-order variables;
        \item \(\evfun(\vec{x})_\pi\) for some event \(\evfun\) and (regular) variables \(\vec{x}\);
        \item or \(\ltlfinally \varphi_1\), where \(\varphi_1\) of one of the above three forms.
      \end{enumerate}
    \end{definition}


    % The advantage of guarded quantifiers in terms of decidability is that they ensure that only a finite number of cases need to be considered regarding the instantiation of \(\vec{\omega}\) when verifying \(\forall \vec{\omega}.\, \varphi_0 \Rightarrow \varphi\), due to the constraints imposed by the guard \(\varphi_0\).
    % We thus consider:

    \begin{definition}[Guarded formula]
      A \pictl[Hyper] formula is said to be \emph{guarded} if all of its quantifiers over regular and second-order variables are either guarded, or of the form \(\forall X.\, \neg X \deduce[\pi] u\).
      % We also require that all second order variables appearing in all atomic formulae of the form \(\InP[X]{Y}_\pi\) or \(\OutP[X]{Y}\) are pairwise distinct.
    \end{definition}

    We stress in particular that \(\exists \vec{\omega}.\, \varphi_0 \wedge \varphi = \neg (\forall \vec{\omega}.\, \varphi_0 \Rightarrow \neg \varphi)\) is also guarded.
    Similarly, \(\forall \vec{\omega}, \neg \varphi_0\) may be interpreted as the guarded formula \(\forall \vec{\omega}, \varphi_0 \Rightarrow \bot\).
    % 
    Our formalisation is purposely very restrictive, and could be easily generalised to broader uses of temporal or logical operators in guards, for example. 
    Our goal was to strengthen our complexity lower bounds to make them relevant w.r.t. future decidability results relying on potentially more permissive definitions.
    All \pictl formulae presented throughout this paper can however be interpreted as guarded, showcasing that it already captures many examples.
    Guarded formulae are however unable to express static equivalence (cf Section~\ref{sec:extensions}: the formula \(\varphi_E\) involves a quantifier alternation).
    We obtain the following results, proved in Appendices~\ref{app:low-pure}, \ref{app:low-subterm} (also valid in the untimed setting).
    % First of all:

    \begin{restatable}{proposition}{propLowerPure} \label{prop:lower-pure}
      For bounded processes of the pure \(\pi\)-calculus and guarded formulae, 
      % the \verif problem is co\np hard in \piltl, and \pspace hard in \pictl and \piltl[Hyper].
      \verif is \pspace hard even in \piltl.
    \end{restatable}

    This result may seem surprising, as several classical reachability properties such as weak secrecy are known to be co\np complete even with some non-trivial term algebras~\cite{RT03}. 
    The \pspace hardness above stems from the possibility to use quantifiers over terms in (guarded) formulae, thus easily permitting to encode the validity of \emph{Quantified Boolean Formulae}, the prototypical \pspace complete problem.
    % Note that, in absence of function symbols, deciding properties of bounded processes mostly amounts to establishing the validity of time conditions.
    % This puts into perspective the following results:
    However, we establish a significantly stronger lower bound when a non-trivial term algebra is additionally used:

    \begin{restatable}{proposition}{propLowerSubterm} \label{prop:lower-subterm}
      For bounded processes of a term algebra with a subterm convergent rewriting system, and for guarded formulae,
      \verif is \exphpoly hard in \pictl and \piltl[Hyper].
    \end{restatable}

    Existing results for trace equivalence already imply the hardness of \piltl[Hyper] w.r.t. some levels of the exponential or polynomial hierarchies, depending on whether subterm convergent rewriting, or none, is used~\cite{CKR20}.
    We therefore non-trivially extend these lower bounds.
    % This recalls the co\nexp completeness of trace equivalence for bounded processes and subterm convergent (destructor) rewriting systems~\cite{CKR20}.
    % We therefore non-trivially extend this lower bound when considering the \piltl[Hyper] fragment.
    % The overall idea is that by representing binary trees in a subterm convergent term algebra, we can encode the truth table of boolean functions, and thus the satisfiability of \emph{Quantified Boolean Second-order Formula}, a complete problem for the exponential hierarchy~\cite{L16}.
    Our negative results are summarised by the following table:

    \input{fig_complexity}

  